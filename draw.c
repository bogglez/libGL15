/**
 * @file draw.c
 * Drawing state and draw calls.
 */

#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <dc/matrix.h>
#include <dc/pvr.h>
#include <dc/video.h>
#include "GL/gl.h"
#include "error.h"
#include "bufferObject.h"
#include "draw.h"
#include "immediate.h"
#include "matrix.h"
#include "texture.h"
#include "util.h"
#include "vao.h"


#define BUFFER_OBJECT_COUNT       64
#define VERTEX_ARRAY_OBJECT_COUNT 64
#define MAX_VERTEX_COUNT          32678

#define VERTEXBUF_SIZE            (1024*1024)


/*
 * State
 */
static unsigned isInitialized = 0;
static float clearColor[4] = {0, 0, 0, 1};

static int    isCullingEnabled = 0;
static GLenum cullFace         = GL_BACK;
static GLenum frontFace        = GL_CCW;

static int    isDepthTestingEnabled = 0;
static GLenum depthFunc             = GL_LESS;

static int    isBlendingEnabled = 0;
static GLenum blendSourceFactor = GL_ONE;
static GLenum blendDestFactor   = GL_ZERO;

static int isTexture2DEnabled = 0;
static Texture * currentTexture2D = NULL;

static GLenum shadeModel = GL_SMOOTH;

static uint32_t currentARGB = 0xFFFFFFFF;
static float currentNormal[3] = {0.f, 0.f, 1.f};

// glGetString strings
static GLubyte const * const vendorString    = (GLubyte const *)"KOS";
static GLubyte const * const rendererString  = (GLubyte const *)"100 MHz PowerVR2 CLX2";
static GLubyte const * const versionString   = (GLubyte const *)"1.5 KOS";
static GLubyte const * const extensionString = (GLubyte const *)
	"GL_ARB_imaging"
	", GL_ARB_vertex_array_object"
	", GL_ARB_vertex_buffer_object";


size_t pvrVertexCounts[PVR_LIST_COUNT] = {0};
pvr_poly_hdr_t polyHeaders[PVR_LIST_COUNT];
static pvr_vertex_t pvrVertices[PVR_LIST_COUNT][MAX_VERTEX_COUNT] ALIGN32;

/*
 * Internal functions
 */

/**
 * Create the polygon headers to submit before sending vertex data.
 */
static void initPVRPolyHeaders() {
	pvr_poly_hdr_t * h = polyHeaders;

	// Initialize common information and copy it to each header
	h->cmd = PVR_CMD_POLYHDR
		| (PVR_USERCLIP_DISABLE  << PVR_TA_CMD_USERCLIP_SHIFT)
		| (PVR_CLRFMT_ARGBPACKED << PVR_TA_CMD_CLRFMT_SHIFT)
		| (PVR_SPECULAR_DISABLE  << PVR_TA_CMD_SPECULAR_SHIFT)
		| (PVR_SHADE_GOURAUD     << PVR_TA_CMD_SHADE_SHIFT)
		| (PVR_UVFMT_32BIT       << PVR_TA_CMD_UVFMT_SHIFT)
		| (PVR_MODIFIER_DISABLE  << PVR_TA_CMD_MODIFIER_SHIFT)
		| (PVR_MODIFIER_NORMAL   << PVR_TA_CMD_MODIFIERMODE_SHIFT);

	h->mode1 =
		  (PVR_DEPTHCMP_ALWAYS   << PVR_TA_PM1_DEPTHCMP_SHIFT)
		| (PVR_CULLING_NONE      << PVR_TA_PM1_CULLING_SHIFT)
		| (PVR_DEPTHWRITE_ENABLE << PVR_TA_PM1_DEPTHWRITE_SHIFT)
		| (PVR_TEXTURE_DISABLE   << PVR_TA_PM1_TXRENABLE_SHIFT);


	h->mode2 =
		  (PVR_BLEND_ONE         << PVR_TA_PM2_SRCBLEND_SHIFT)
		| (PVR_BLEND_ZERO        << PVR_TA_PM2_DSTBLEND_SHIFT)
		| (PVR_FOG_DISABLE       << PVR_TA_PM2_FOG_SHIFT)
		| (PVR_CLRCLAMP_DISABLE  << PVR_TA_PM2_CLAMP_SHIFT)
		| (PVR_FILTER_NEAREST    << PVR_TA_PM2_FILTER_SHIFT)
		| (PVR_MIPBIAS_NORMAL    << PVR_TA_PM2_MIPBIAS_SHIFT)
		| (PVR_UVFLIP_NONE       << PVR_TA_PM2_UVFLIP_SHIFT);

	h->mode3 = PVR_MIPMAP_DISABLE << PVR_TA_PM3_MIPMAP_SHIFT;
	h->d1    = 0xffffffff;
	h->d2    = 0xffffffff;
	h->d3    = 0xffffffff;
	h->d4    = 0xffffffff;

	for(int i = 1; i < PVR_LIST_COUNT; ++i) {
		memcpy(polyHeaders + i, h, sizeof(pvr_poly_hdr_t));
	}

	// Set type for each poly header
	for(int i = 0; i < PVR_LIST_COUNT; ++i) {
		polyHeaders[i].cmd |= i << PVR_TA_CMD_TYPE_SHIFT;
	}

	unsigned const opaqueMode2 =
		  (PVR_ALPHA_DISABLE    << PVR_TA_PM2_ALPHA_SHIFT)
		| (PVR_BLEND_DISABLE    << PVR_TA_PM2_SRCENABLE_SHIFT)
		| (PVR_BLEND_DISABLE    << PVR_TA_PM2_DSTENABLE_SHIFT)
		| (PVR_TXRALPHA_DISABLE << PVR_TA_PM2_TXRALPHA_SHIFT)
		| (PVR_TXRENV_MODULATE  << PVR_TA_PM2_TXRENV_SHIFT);

	unsigned const translucentMode2 =
		  (PVR_ALPHA_ENABLE         << PVR_TA_PM2_ALPHA_SHIFT)
		| (PVR_BLEND_ENABLE         << PVR_TA_PM2_SRCENABLE_SHIFT)
		| (PVR_BLEND_ENABLE         << PVR_TA_PM2_DSTENABLE_SHIFT)
		| (PVR_TXRALPHA_ENABLE      << PVR_TA_PM2_TXRALPHA_SHIFT)
		| (PVR_TXRENV_MODULATEALPHA << PVR_TA_PM2_TXRENV_SHIFT);

	for(int i = 0; i < PVR_LIST_TR_POLY; ++i) {
		pvr_poly_hdr_t * h = &polyHeaders[i];
		h->mode2 |= opaqueMode2;
	}

	for(int i = PVR_LIST_TR_POLY; i < PVR_LIST_COUNT; ++i) {
		pvr_poly_hdr_t * h = &polyHeaders[i];
		h->mode2 |= translucentMode2;
	}
}

/**
 * Resets the state of the vertex buffers (opaque, transparent).
 * This function should be called on a new frame.
 */
static void resetVertexBuffers() {
	memset(pvrVertexCounts, 0, sizeof(pvrVertexCounts));
}

#ifndef NDEBUG
/** Checks whether the provided GLenum is a depth function name.
 * @param func The GLenum to check.
 * @return 1 if func is a depth function name, 0 otherwise.
 */
static int isValidDepthFunc(GLenum func) {
	return func == GL_NEVER
		|| func == GL_LESS
		|| func == GL_EQUAL
		|| func == GL_LEQUAL
		|| func == GL_GREATER
		|| func == GL_NOTEQUAL
		|| func == GL_GEQUAL
		|| func == GL_ALWAYS;
}

static int isValidBlendSourceFactor(GLenum sfactor) {
	return sfactor == GL_ZERO
		|| sfactor == GL_ONE
		|| sfactor == GL_SRC_COLOR
		|| sfactor == GL_ONE_MINUS_SRC_COLOR
		|| sfactor == GL_DST_COLOR
		|| sfactor == GL_ONE_MINUS_DST_COLOR
		|| sfactor == GL_SRC_ALPHA
		|| sfactor == GL_ONE_MINUS_SRC_ALPHA
		|| sfactor == GL_DST_ALPHA
		|| sfactor == GL_ONE_MINUS_DST_ALPHA
		|| sfactor == GL_CONSTANT_COLOR
		|| sfactor == GL_ONE_MINUS_CONSTANT_COLOR
		|| sfactor == GL_CONSTANT_ALPHA
		|| sfactor == GL_ONE_MINUS_CONSTANT_ALPHA
		|| sfactor == GL_SRC_ALPHA_SATURATE;
}

static int isValidBlendDestFactor(GLenum dfactor) {
	return dfactor == GL_ZERO
		|| dfactor == GL_ONE
		|| dfactor == GL_SRC_COLOR
		|| dfactor == GL_ONE_MINUS_SRC_COLOR
		|| dfactor == GL_DST_COLOR
		|| dfactor == GL_ONE_MINUS_DST_COLOR
		|| dfactor == GL_SRC_ALPHA
		|| dfactor == GL_ONE_MINUS_SRC_ALPHA
		|| dfactor == GL_DST_ALPHA
		|| dfactor == GL_ONE_MINUS_DST_ALPHA
		|| dfactor == GL_CONSTANT_COLOR
		|| dfactor == GL_ONE_MINUS_CONSTANT_COLOR
		|| dfactor == GL_CONSTANT_ALPHA
		|| dfactor == GL_ONE_MINUS_CONSTANT_ALPHA;
}

/**
 * Checks whether the GLenum describes an OpenGL capability.
 * @param cap The GLenum to check.
 * @return 1 if cap describes an OpenGL capability, 0 otherwise.
 */
static int isValidCapability(GLenum cap) {
	return cap == GL_ALPHA_TEST
		|| cap == GL_AUTO_NORMAL
		|| cap == GL_BLEND
		|| (cap >= GL_CLIP_PLANE0 && cap <= GL_CLIP_PLANE5)
		|| cap == GL_COLOR_LOGIC_OP
		|| cap == GL_COLOR_MATERIAL
		|| cap == GL_COLOR_SUM
		|| cap == GL_COLOR_TABLE
		|| cap == GL_CONVOLUTION_1D
		|| cap == GL_CONVOLUTION_2D
		|| cap == GL_CULL_FACE
		|| cap == GL_DEPTH_TEST
		|| cap == GL_DITHER
		|| cap == GL_FOG
		|| cap == GL_HISTOGRAM
		|| cap == GL_INDEX_LOGIC_OP
		|| (cap >= GL_LIGHT0 && cap <= GL_LIGHT7)
		|| cap == GL_LIGHTING
		|| cap == GL_LINE_SMOOTH
		|| cap == GL_LINE_STIPPLE
		|| cap == GL_MAP1_COLOR_4
		|| cap == GL_MAP1_INDEX
		|| cap == GL_MAP1_NORMAL
		|| (cap >= GL_MAP1_TEXTURE_COORD_1 && cap <= GL_MAP1_TEXTURE_COORD_4)
		|| cap == GL_MAP1_VERTEX_3
		|| cap == GL_MAP1_VERTEX_4
		|| cap == GL_MAP2_COLOR_4
		|| cap == GL_MAP2_INDEX
		|| cap == GL_MAP2_NORMAL
		|| (cap >= GL_MAP2_TEXTURE_COORD_1 && cap <= GL_MAP1_TEXTURE_COORD_4)
		|| cap == GL_MAP2_VERTEX_3
		|| cap == GL_MAP2_VERTEX_4
		|| cap == GL_MINMAX
		|| cap == GL_MULTISAMPLE
		|| cap == GL_NORMALIZE
		|| cap == GL_POINT_SMOOTH
		|| cap == GL_POLYGON_OFFSET_FILL
		|| cap == GL_POLYGON_OFFSET_LINE
		|| cap == GL_POLYGON_OFFSET_POINT
		|| cap == GL_POLYGON_SMOOTH
		|| cap == GL_POLYGON_STIPPLE
		|| cap == GL_POST_COLOR_MATRIX_COLOR_TABLE
		|| cap == GL_POST_CONVOLUTION_COLOR_TABLE
		|| cap == GL_RESCALE_NORMAL
		|| cap == GL_SAMPLE_ALPHA_TO_COVERAGE
		|| cap == GL_SAMPLE_ALPHA_TO_ONE
		|| cap == GL_SAMPLE_COVERAGE
		|| cap == GL_SEPARABLE_2D
		|| cap == GL_SCISSOR_TEST
		|| cap == GL_STENCIL_TEST
		|| cap == GL_TEXTURE_1D
		|| cap == GL_TEXTURE_2D
		|| cap == GL_TEXTURE_3D
		|| cap == GL_TEXTURE_CUBE_MAP
		|| cap == GL_TEXTURE_GEN_Q
		|| cap == GL_TEXTURE_GEN_R
		|| cap == GL_TEXTURE_GEN_S
		|| cap == GL_TEXTURE_GEN_T;
}

/**
 * Checks whether the GLenum describes an OpenGL hint target.
 * @param target The GLenum to check.
 * @return 1 if target is an OpenGL hint target, otherwise 0.
 */
static int isValidHintTarget(GLenum target) {
	return target == GL_FOG_HINT
		|| target == GL_GENERATE_MIPMAP_HINT
		|| target == GL_LINE_SMOOTH_HINT
		|| target == GL_PERSPECTIVE_CORRECTION_HINT
		|| target == GL_POINT_SMOOTH_HINT
		|| target == GL_POLYGON_SMOOTH_HINT
		|| target == GL_TEXTURE_COMPRESSION_HINT;
}

/**
 * Checks whether the provided GLenum describes an OpenGL string.
 * @param name The GLenum to check.
 * @return 1 if it is an OpenGL string, 0 otherwise.
 */
static int isValidStringName(GLenum name) {
	return name == GL_VENDOR
		|| name == GL_RENDERER
		|| name == GL_VERSION
		|| name == GL_EXTENSIONS;
}
#endif

/**
 * Converts depth function names from OpenGL to PVR.
 * Note that the OpenGL and PVR depth functions are complementary to each other,
 * so a GL_LESS is turned into a PVR_DEPTHCMP_GEQUAL.
 * @param func The depth function's name as GLenum.
 * @return The PVR function corresponding to the GLenum.
 */
static unsigned glDepthFuncToPVR(GLenum func) {
	switch(func) {
		case GL_NEVER:    return PVR_DEPTHCMP_NEVER;
		case GL_LESS:     return PVR_DEPTHCMP_GEQUAL;
		case GL_EQUAL:    return PVR_DEPTHCMP_EQUAL;
		case GL_LEQUAL:   return PVR_DEPTHCMP_GREATER;
		case GL_GREATER:  return PVR_DEPTHCMP_LEQUAL;
		case GL_NOTEQUAL: return PVR_DEPTHCMP_NOTEQUAL;
		case GL_GEQUAL:   return PVR_DEPTHCMP_LESS;
		case GL_ALWAYS:   return PVR_DEPTHCMP_ALWAYS;
	}

	assert(0);
	return GL_NEVER;
}

/**
 * Converts shade model names from OpenGL to PVR.
 * @param model The name of the shade model.
 * @return THe PVR shade model corresponding to the GLenum.
 */
static unsigned glShadeModelToPVR(GLenum model) {
	switch(model) {
		case GL_FLAT:   return PVR_SHADE_FLAT;
		case GL_SMOOTH: return PVR_SHADE_GOURAUD;
	}

	assert(0);
	return PVR_SHADE_GOURAUD;
}

/**
 * Converts an OpenGL (source or destination) blend factor to the PVR
 * equivalent, if it exists.
 * @param factor The OpenGL blend factor to convert.
 * @param fallbackFactor The PVR blend factor to use when no conversion is possible.
 * @return The PVR equivalent blend factor, or fallbackFactor if it does not exist.
 */
static unsigned glBlendFactorToPVR(GLenum factor, unsigned const fallbackFactor) {
	assert(isValidBlendSourceFactor(factor) || isValidBlendDestFactor(factor));

	switch(factor) {
		case GL_ZERO:                return PVR_BLEND_ZERO;
		case GL_ONE:                 return PVR_BLEND_ONE;
		case GL_DST_COLOR:           return PVR_BLEND_DESTCOLOR;
		case GL_ONE_MINUS_DST_COLOR: return PVR_BLEND_INVDESTCOLOR;
		case GL_SRC_ALPHA:           return PVR_BLEND_SRCALPHA;
		case GL_ONE_MINUS_SRC_ALPHA: return PVR_BLEND_INVSRCALPHA;
		case GL_DST_ALPHA:           return PVR_BLEND_DESTALPHA;
		case GL_ONE_MINUS_DST_ALPHA: return PVR_BLEND_INVDESTALPHA;

		case GL_SRC_COLOR:
		case GL_ONE_MINUS_SRC_COLOR:
		case GL_CONSTANT_COLOR:
		case GL_ONE_MINUS_CONSTANT_COLOR:
		case GL_CONSTANT_ALPHA:
		case GL_ONE_MINUS_CONSTANT_ALPHA:
		case GL_SRC_ALPHA_SATURATE:
		glSetError(GL_KOS_UNSUPPORTED);
		break;

		// Missing blend factor
		default: assert(0);
	}

#ifndef NDEBUG
	glSetError(GL_KOS_UNSUPPORTED);
#endif

	return fallbackFactor;
}

/**
 * Determines which polygon list to use, depending on what blending mode is used.
 * \return The number of the polygon list that should be used.
 */
static int determinePolygonList() {
	if(!isBlendingEnabled) {
		return PVR_LIST_OP_POLY;
	}
	// Punch-through is used for alpha testing
	else if (blendSourceFactor == GL_ONE && blendDestFactor == GL_ZERO) {
		return PVR_LIST_PT_POLY;
	}

	return PVR_LIST_TR_POLY;
}

/**
 * Converts a pair of OpenGL cull face and front face to a PVR culling mode.
 * In OpenGL it is possible to define which vertex order describes forward facing
 * triangles, in addition to setting whether the front or the back of a triangle
 * should be culled. The PVR does not accept setting the vertex order, so this
 * conversion becomes necessary.
 * @param cullFace Whether the front or back should be culled.
 * @param frontFace Whether the vertex order of forward-facing triangles is counter-clockwise or clockwise.
 */
static unsigned glCullToPVR(GLenum newCullFace, GLenum newFrontFace) {
	if((newCullFace == GL_BACK && newFrontFace == GL_CCW)
		|| (newCullFace == GL_FRONT && newFrontFace == GL_CW)) {
		return PVR_CULLING_CW;
	}
	else if((newCullFace == GL_BACK && newFrontFace == GL_CW)
		|| (newCullFace == GL_FRONT && newFrontFace == GL_CCW)) {
		return PVR_CULLING_CCW;
	}
	else if(newCullFace == GL_FRONT_AND_BACK) {
		/// \todo GL_FRONT_AND_BACK causes only polys and lines to be drawn..
#ifndef NDEBUG
		glSetError(GL_KOS_UNIMPLEMENTED);
#endif
		return PVR_CULLING_CCW;
	}

	assert(0);
	return PVR_CULLING_CCW;
}

static void setBlendMode(int isActive, GLenum source, GLenum dest) {
	blendSourceFactor = source;
	blendDestFactor   = dest;
	isBlendingEnabled = isActive;

	int const pvrSource = glBlendFactorToPVR(source, GL_ONE)  << PVR_TA_PM2_SRCBLEND_SHIFT;
	int const pvrDest   = glBlendFactorToPVR(dest,   GL_ZERO) << PVR_TA_PM2_DSTBLEND_SHIFT;
	int const mode      = pvrSource | pvrDest;

	unsigned const clearMask = ~(PVR_TA_PM2_SRCBLEND_MASK | PVR_TA_PM2_DSTBLEND_MASK);
	polyHeaders[PVR_LIST_TR_POLY].mode2 = (polyHeaders[PVR_LIST_TR_POLY].mode2 & clearMask) | mode;
	polyHeaders[PVR_LIST_TR_MOD] .mode2 = (polyHeaders[PVR_LIST_TR_MOD] .mode2 & clearMask) | mode;
}

static void setCullMode(int isActive, GLenum newCullFace, GLenum newFrontFace) {
	cullFace  = newCullFace;
	frontFace = newFrontFace;
	isCullingEnabled = isActive;

	unsigned mode = isActive ? glCullToPVR(cullFace, frontFace) : PVR_CULLING_NONE;
	mode <<= PVR_TA_PM1_CULLING_SHIFT;

	for(int i = 0; i < PVR_LIST_COUNT; ++i) {
		polyHeaders[i].mode1 = (polyHeaders[i].mode1 & ~PVR_TA_PM1_CULLING_MASK ) | mode;
	}
}

static void setDepthMode(int isActive, int newDepthFunc) {
	depthFunc = newDepthFunc;
	isDepthTestingEnabled = isActive;

	unsigned mode = isActive ? glDepthFuncToPVR(depthFunc) : PVR_DEPTHCMP_ALWAYS;
	mode <<= PVR_TA_PM1_DEPTHCMP_SHIFT;
	for(int i = 0; i < PVR_LIST_COUNT; ++i) {
		polyHeaders[i].mode1 = (polyHeaders[i].mode1 & ~PVR_TA_PM1_DEPTHCMP_MASK) | mode;
	}
}

static unsigned log2u(unsigned v) {
	unsigned result = 0;
	while(v >>= 1) {
		++result;
	}
	return result;
}

static int isValidTextureDimension(unsigned v) {
	return v && v <= 1024 && !(v & (v-1));
}

/**
 * Converts the texture dimensions into PVR format.
 * PVR format specifies 8->0, 16->1, 32->2, 64->3, 128->4, 256->5, 512->6, 1024->7.
 * This conversion is required to add the dimensions to a polygon header.
 * \return Converted width and height packed together.
 */
static unsigned textureDimensionsToPVR(unsigned w, unsigned h) {
	assert(isValidTextureDimension(w));
	assert(isValidTextureDimension(h));
	return (log2u(w / 8) << PVR_TA_PM2_USIZE_SHIFT)
		 | (log2u(h / 8) << PVR_TA_PM2_VSIZE_SHIFT);
}

/**
 * Mark a texture for usage during texture mode and activate it if texturing is enabled.
 * \param isActive Whether texturing should be on or off.
 * \param texture  The texture to be set up if texturing is enabled.
 */
static void setTextureMode(int isActive, Texture * texture) {
	/// \fixme - What's the OpenGL behavior when texturing is enabled but no texture is set?

	isTexture2DEnabled = isActive;
	currentTexture2D   = texture;

	// Disable texturing
	unsigned const clearMask = ~(
		  PVR_TA_PM2_FILTER_MASK
		| PVR_TA_PM2_TXRALPHA_MASK
//		| PVR_TA_PM2_TXRENV_MASK
		| PVR_TA_PM2_MIPBIAS_MASK
//		| PVR_TA_PM2_UVFLIP_MASK
//		| PVR_TA_PM2_UVCLAMP_MASK
		| PVR_TA_PM2_USIZE_MASK
		| PVR_TA_PM2_VSIZE_MASK);

	for(int i = 0; i < PVR_LIST_COUNT; ++i) {
		pvr_poly_hdr_t * h = &polyHeaders[i];
		h->cmd   &= ~8;
		h->mode1 &= ~PVR_TA_PM1_TXRENABLE_MASK;
		h->mode2 &= clearMask;
		h->mode3 = 0;
	}

	// Activate texture
	if(isTexture2DEnabled && texture) {
/// \todo - Set up parameters from texture unit
//		TextureUnit const * unit = getActiveTextureUnit();

		for(int i = 0; i < PVR_LIST_COUNT; ++i) {
			pvr_poly_hdr_t * h = &polyHeaders[i];
			h->cmd   |= 8;

			h->mode1 |= 1 << PVR_TA_PM1_TXRENABLE_SHIFT;
			h->mode2 |= (texture->filter << PVR_TA_PM2_FILTER_SHIFT); 

			h->mode3 |= ((((uint32_t)texture->data) & 0x00fffff8) >> 3)
				| (convertGLTextureFormatToPVR(texture->internalFormat) << PVR_TA_PM3_TXRFMT_SHIFT);

			// Texture dimensions may be 0 immediately after glBindTexture2D
			if(texture->width || texture->height) {
				h->mode2 |= textureDimensionsToPVR(texture->width, texture->height);
			}
		}
	}
}

/**
 * Enables or disables an OpenGL capability (glEnable, glDisable).
 * @param cap The OpenGL capability to enable or disable.
 * @param isActive Whether the OpenGL capability should be active or not.
 */
static void setCapabilityState(GLenum cap, int isActive) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidCapability(cap));


	switch(cap) {
		case GL_CULL_FACE:
			setCullMode(isActive, cullFace, frontFace);
			break;

		case GL_DEPTH_TEST:
			setDepthMode(isActive, depthFunc);
			break;

		case GL_TEXTURE_2D:
			setTextureMode(isActive, currentTexture2D);
			break;

		case GL_BLEND:
			setBlendMode(isActive, blendSourceFactor, blendDestFactor);
			break;


		default:
			/// \todo Many OpenGL capabilities are unsupported.
#ifndef NDEBUG
			glSetError(GL_KOS_UNIMPLEMENTED);
#endif
			return;
	}
}

/**
 * Draws a triangle strip without any additional vertex attributes apart from the vertex coordinate.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTriStripV3F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride % sizeof(float)));

	unsigned const       vertexStride = attribs->vertexStride / sizeof(float);
	float const *        vertex       = attribs->vertex;
	pvr_vertex_t * const destEnd      = dest + count;

	loadModelToWindowMatrixToRegisters();

	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], vertex[2], dest->x, dest->y, dest->z);
		dest->argb = currentARGB;

		++dest;
		vertex += vertexStride;
	}

	// set EOL flag on last vertex
	(destEnd-1)->flags = PVR_CMD_VERTEX_EOL;

	return count;
}

/**
 * Draws a triangle strip without any additional vertex attributes apart from the vertex coordinate.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTriStripV2F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride % sizeof(float)));

	unsigned const       vertexStride = attribs->vertexStride / sizeof(float);
	float const *        vertex       = attribs->vertex;
	pvr_vertex_t * const destEnd      = dest + count;

	loadModelToWindowMatrixToRegisters();

	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], 0.f, dest->x, dest->y, dest->z);
		dest->argb = currentARGB;

		++dest;
		vertex += vertexStride;
	}

	// set EOL flag on last vertex
	(destEnd-1)->flags = PVR_CMD_VERTEX_EOL;

	return count;
}

/**
 * Draws a triangle strip with coordinate and color vertex attributes enabled.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTriStripV3F_C4F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride % sizeof(float)));
	assert(!(attribs->colorStride  % sizeof(float)));

	unsigned const       vertexStride = attribs->vertexStride / sizeof(float);
	unsigned const       colorStride  = attribs->colorStride  / sizeof(float);
	float const *        vertex       = attribs->vertex;
	float const *        color        = attribs->color;
	pvr_vertex_t * const destEnd      = dest + count;

	loadModelToWindowMatrixToRegisters();

	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], vertex[2], dest->x, dest->y, dest->z);
		dest->argb  = PVR_PACK_COLOR(color[3], color[0], color[1], color[2]);

		++dest;
		vertex += vertexStride;
		color  += colorStride;

	}

	// set EOL flag on last vertex
	(destEnd-1)->flags = PVR_CMD_VERTEX_EOL;

	return count;
}

/**
 * Draws a triangle strip with vertex coordinate and texture coordinate attributes enabled.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTriStripV2F_T2F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride  % sizeof(float)));
	assert(!(attribs->texCoordStride % sizeof(float)));

	unsigned const       vertexStride    = attribs->vertexStride   / sizeof(float);
	unsigned const       texCoordStride  = attribs->texCoordStride / sizeof(float);
	float const *        vertex          = attribs->vertex;
	float const *        texCoord        = attribs->texCoord;
	pvr_vertex_t * const destEnd         = dest + count;

	loadModelToWindowMatrixToRegisters();

	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], 0.f, dest->x, dest->y, dest->z);
		dest->argb = currentARGB;
		dest->u = texCoord[0];
		dest->v = texCoord[1];

		++dest;
		vertex   += vertexStride;
		texCoord += texCoordStride;
	}

	// set EOL flag on last vertex
	(destEnd-1)->flags = PVR_CMD_VERTEX_EOL;

	return count;
}


/**
 * Draws a triangle strip with vertex coordinate and texture coordinate attributes enabled.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTriStripV3F_T2F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride   % sizeof(float)));
	assert(!(attribs->texCoordStride % sizeof(float)));

	unsigned const       vertexStride   = attribs->vertexStride   / sizeof(float);
	unsigned const       texCoordStride = attribs->texCoordStride / sizeof(float);
	float const *        vertex         = attribs->vertex;
	float const *        texCoord       = attribs->texCoord;
	pvr_vertex_t * const destEnd        = dest + count;

	loadModelToWindowMatrixToRegisters();

	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], vertex[2], dest->x, dest->y, dest->z);
		dest->argb = currentARGB;
		dest->u = texCoord[0];
		dest->v = texCoord[1];

		++dest;
		vertex   += vertexStride;
		texCoord += texCoordStride;
	}

	// set EOL flag on last vertex
	(destEnd-1)->flags = PVR_CMD_VERTEX_EOL;

	return count;
}

/**
 * Draws a triangle strip with vertex coordinate and texture coordinate attributes enabled.
 * @param attribs Vertex attribute information to be used during the draw call (data source and stride).
 * @param count   How many vertices should be rendered starting from coord.
 * @param dest    The PVR command list into which to store the transformed vertices.
 * @return How many PVR vertex commands were added.
 */
static size_t drawTrisV3F_T2F(VertexAttribs * const attribs, GLsizei count, pvr_vertex_t *dest) {
	/// \todo Implement clipping.
	assert(!(attribs->vertexStride   % sizeof(float)));
	assert(!(attribs->texCoordStride % sizeof(float)));

	unsigned const       vertexStride   = attribs->vertexStride   / sizeof(float);
	unsigned const       texCoordStride = attribs->texCoordStride / sizeof(float);
	float const *        vertex         = attribs->vertex;
	float const *        texCoord       = attribs->texCoord;
	pvr_vertex_t * const destEnd        = dest + count;
	pvr_vertex_t * const originalDest   = dest;

	loadModelToWindowMatrixToRegisters();


	while(dest != destEnd) {
		dest->flags = PVR_CMD_VERTEX;
		mat_trans_single3_nomod(vertex[0], vertex[1], vertex[2], dest->x, dest->y, dest->z);
		dest->argb = currentARGB;
		dest->u = texCoord[0];
		dest->v = texCoord[1];

		++dest;
		vertex   += vertexStride;
		texCoord += texCoordStride;
	}

	for(dest = originalDest + 2; dest < destEnd; dest += 3) {
		dest->flags = PVR_CMD_VERTEX_EOL;
	}

	return count;
}


/*
 * Functions
 */

#ifndef NDEBUG
/**
 * Checks whether the provided GLenum is a draw mode.
 * @param mode The GLenum to check.
 * @return 1 if mode is a draw mode, 0 otherwise.
 */
int isValidDrawMode(GLenum mode) {
	return mode == GL_POINTS
		|| mode == GL_LINE_STRIP
		|| mode == GL_LINE_LOOP
		|| mode == GL_LINES
		|| mode == GL_TRIANGLE_STRIP
		|| mode == GL_TRIANGLE_FAN
		|| mode == GL_TRIANGLES
		|| mode == GL_QUAD_STRIP
		|| mode == GL_QUADS
		|| mode == GL_POLYGON;
}
#endif

void useTexture(Texture * texture) {
	setTextureMode(isTexture2DEnabled, texture);
}


/*
 * OpenGL functions
 */

/**
 * Initializes the OpenGL implementation.
 * Should be called before usage of OpenGL.
 */
void glKosInit() {
#ifndef NDEBUG
	int error;
#endif

	assert(!isInitialized);

	unsigned short const screenWidth = 640;
	unsigned short const screenHeight = 480;
	vid_set_mode(DM_640x480, PM_RGB565);

	pvr_init_params_t params = {
		// bins: opaque polygons, opaque modifiers, translucent polygons, translucent modifiers, punch-thrus */
		{ PVR_BINSIZE_32, PVR_BINSIZE_0, PVR_BINSIZE_32, PVR_BINSIZE_0, PVR_BINSIZE_0 },
		// Vertex buffer size (should be a nice round number)
		VERTEXBUF_SIZE, // Vertex buffer size
		0, // DMA
#ifdef LIBGL15_FSAA
		1, // FSAA
#else
		0, // no FSAA
#endif
		0, // autosort disabled
	};

#ifndef NDEBUG
	error =
#endif
		pvr_init(&params);
	assert(!error);

	pvr_set_pal_format(PVR_PAL_ARGB8888);


	pvr_scene_begin();

#ifndef NDEBUG
	error =
#endif
		initBufferObjects(BUFFER_OBJECT_COUNT);
	assert(!error);

#ifndef NDEBUG
	error =
#endif
		initVertexArrayObjects(VERTEX_ARRAY_OBJECT_COUNT);
	assert(!error);

	initTextures();
	initTextureUnits();

	initPVRPolyHeaders();

	glViewport(0, 0, screenWidth, screenHeight);
	glDepthRange(0, 1);

#ifndef NDEBUG
	error = glGetError();
	assert(!error);
#endif

	isInitialized = 1;
}

void GLAPIENTRY glFlush() {
	pvr_wait_ready();
}

void GLAPIENTRY glFinish() {
	pvr_wait_ready();
}


// glColor
void GLAPIENTRY glColor4ub(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha) {
	currentARGB = blue | (green << 8) | (red << 16) | (alpha << 24);
}

void GLAPIENTRY glColor4ubv(GLubyte const *v) {
	glColor4ub(v[0], v[1], v[2], v[3]);
}


void GLAPIENTRY glColor3ub(GLubyte red, GLubyte green, GLubyte blue) {
	glColor4ub(red, green, blue, 0xff);
}

void GLAPIENTRY glColor3ubv(GLubyte const *v) {
	glColor3ub(v[0], v[1], v[2]);
}


void GLAPIENTRY glColor4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha) {
	/// \todo According to the spec no clamping should be done here, but during render. Reverify this when implementing blending.
	red   = CLAMP1(red);
	green = CLAMP1(green);
	blue  = CLAMP1(blue);
	alpha = CLAMP1(alpha);

	currentARGB = PVR_PACK_COLOR(alpha, red, green, blue);
}

void GLAPIENTRY glColor4fv(GLfloat const *v) {
	glColor4f(v[0], v[1], v[2], v[3]);
}

void GLAPIENTRY glColor3f(GLfloat red, GLfloat green, GLfloat blue) {
	glColor4f(red, green, blue, 1.f);
}

void GLAPIENTRY glColor3fv(GLfloat const *v) {
	glColor3f(v[0], v[1], v[2]);
}

void GLAPIENTRY glColor4d(GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha) {
	glColor4f(red, green, blue, alpha);
}

void GLAPIENTRY glColor4dv(GLdouble const *v) {
	glColor4d(v[0], v[1], v[2], v[3]);
}

// glNormal
void GLAPIENTRY glNormal3f(GLfloat nx, GLfloat ny, GLfloat nz) {
	currentNormal[0] = nx;
	currentNormal[1] = ny;
	currentNormal[2] = nz;
}

/// \fixme Negative integer normal values are not mapped to -1.0.
void GLAPIENTRY glNormal3b(GLbyte nx, GLbyte ny, GLbyte nz) {
	glNormal3f(nx/(float)INT8_MAX, ny/(float)INT8_MAX, nz/(float)INT8_MAX);
}

void GLAPIENTRY glNormal3d(GLdouble nx, GLdouble ny, GLdouble nz) {
	glNormal3f(nx, ny, nz);
}

/// \fixme Negative integer normal values are not mapped to -1.0.
void GLAPIENTRY glNormal3i(GLint nx, GLint ny, GLint nz) {
	glNormal3f(nx/(float)INT32_MAX, ny/(float)INT32_MAX, nz/(float)INT32_MAX);
}


/// \fixme Negative integer normal values are not mapped to -1.0.
void GLAPIENTRY glNormal3s(GLshort nx, GLshort ny, GLshort nz) {
	glNormal3f(nx/(float)INT16_MAX, ny/(float)INT16_MAX, nz/(float)INT16_MAX);
}


/// \fixme Negative integer normal values are not mapped to -1.0.
void GLAPIENTRY glNormal3bv(GLbyte const *v) {
	glNormal3b(v[0], v[1], v[2]);
}

void GLAPIENTRY glNormal3dv(GLdouble const *v) {
	glNormal3d(v[0], v[1], v[2]);
}

void GLAPIENTRY glNormal3fv(GLfloat const *v) {
	glNormal3f(v[0], v[1], v[2]);
}

void GLAPIENTRY glNormal3iv(GLint const *v) {
	glNormal3i(v[0], v[1], v[2]);
}

void GLAPIENTRY glNormal3sv(GLshort const *v) {
	glNormal3s(v[0], v[1], v[2]);
}


// Clearing
void GLAPIENTRY glClearColor(GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	clearColor[0] = CLAMP1(red);
	clearColor[1] = CLAMP1(green);
	clearColor[2] = CLAMP1(blue);
	clearColor[3] = CLAMP1(alpha);
}

void GLAPIENTRY glClear(GLbitfield mask) {
	GL_CHECK(GL_INVALID_VALUE, mask & ~(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


#ifndef NDEBUG
	/// \todo - No depth buffer on PVR, need to work around this somehow?

	if(mask & (GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT)) {
		/// \todo - No accum buffer or stencil buffer.
		glSetError(GL_KOS_UNIMPLEMENTED);
		return;
	}
#endif

	if(mask & GL_COLOR_BUFFER_BIT) {
		pvr_set_bg_color(clearColor[0], clearColor[1], clearColor[2]);
	}
}

/// \todo Custom sq_cpy by ph3nom, need to profile this
#define TA_SQ_ADDR (unsigned int *)(void *)(0xe0000000 | (((unsigned long)0x10000000) & 0x03ffffe0))
static inline void pvr_list_submit(void *src, size_t n) {
    unsigned int *d = TA_SQ_ADDR;
    unsigned int *s = src;

    // fill/write queues as many times necessary
    ++n;
    while(--n) {
        asm("pref @%0" : : "r"(s + 8));  /* prefetch 32 bytes for next loop */
        d[0] = *(s++);
        d[1] = *(s++);
        d[2] = *(s++);
        d[3] = *(s++);
        d[4] = *(s++);
        d[5] = *(s++);
        d[6] = *(s++);
        d[7] = *(s++);
        asm("pref @%0" : : "r"(d));
        d += 8;
    }

    // Wait for both store queues to complete
    d = (unsigned int *)0xe0000000;
    d[0] = d[8] = 0;
}

void APIENTRY glSwapBuffersKOS() {
#ifndef NDEBUG
	int error;
#endif

	pvr_scene_begin();

	for(int i = 0; i < PVR_LIST_COUNT; ++i) {
		size_t const count = pvrVertexCounts[i];
		if(count) {
			pvr_list_submit(pvrVertices[i], count);
		}
	}

#ifndef NDEBUG
	error =
#endif
		pvr_scene_finish();
	assert(!error);

	glFlush();
	pvr_scene_begin();
	resetVertexBuffers();
}

void GLAPIENTRY glDrawArrays(GLenum mode, GLint first, GLsizei count) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_VALUE, count < 0);


	/// \todo GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to an
	/// enabled array and the buffer object's data store is currently mapped.

	glMultiDrawArrays(mode, &first, &count, 1);
}

void APIENTRY glMultiDrawArrays(GLenum mode, GLint const *firsts, GLsizei const *counts, GLsizei drawcount) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_VALUE, drawcount < 0);

	/// \todo - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to an enabled array and the buffer object's data store is currently mapped.

/// \todo - Check remaining space in list

	VertexArrayObject const * const vao = getActiveVertexArrayObject();
	updateCachedMatrices();

	// It makes no sense to call this without any enabled vertex attributes.
	assert(vao->enabledAttribs);

	// Decide which polygon list to use
	int const list = determinePolygonList();
	size_t * listVertCount = &pvrVertexCounts[list];
	pvr_vertex_t * dest = &pvrVertices[list][*listVertCount];

	// Add poly header before drawing buffers
	memcpy(dest, &polyHeaders[list], sizeof(pvr_poly_hdr_t));
	++dest;
	++*listVertCount;

	while(drawcount--) {
		GLint const first = *firsts++;
		GLint const count = *counts++;
		size_t addedPVRCommands = 0;

		// Make a copy of the vertex attributes and apply offsets for this draw call
		VertexAttribs attribs = vao->attribs;
		attribs.vertex         += first * attribs.vertexStride;
		attribs.normal         += first * attribs.normalStride;
		attribs.texCoord       += first * attribs.texCoordStride;
		attribs.color          += first * attribs.colorStride;
		attribs.secondaryColor += first * attribs.secondaryColorStride;
		attribs.colorIndex     += first * attribs.colorIndexStride;
		attribs.edgeFlag       += first * attribs.edgeFlagStride;
		attribs.fogCoord       += first * attribs.fogCoordStride;

/// \todo - Implement usage of remaining vertex attributes during draw calls.

		// Drawing depends on drawing mode and what attributes are enabled (want to avoid branching in the loop)
		if(mode == GL_TRIANGLE_STRIP
			&& vao->enabledAttribs == (ENABLEDVERTEXATTRIB_VERTEX | ENABLEDVERTEXATTRIB_TEXCOORD0)
			&& attribs.vertexType == GL_FLOAT
			&& attribs.texCoordType == GL_FLOAT
			&& attribs.vertexComponents == 3
			&& attribs.texCoordComponents == 2
		) {
			addedPVRCommands = drawTriStripV3F_T2F(&attribs, count, dest);
		}

		else if(mode == GL_TRIANGLE_STRIP
			&& vao->enabledAttribs == ENABLEDVERTEXATTRIB_VERTEX
			&& attribs.vertexType == GL_FLOAT
			&& attribs.vertexComponents == 3
		) {
			addedPVRCommands = drawTriStripV3F(&attribs, count, dest);
		}

		else if(mode == GL_TRIANGLE_STRIP
			&& vao->enabledAttribs == (ENABLEDVERTEXATTRIB_VERTEX | ENABLEDVERTEXATTRIB_COLOR)
			&& attribs.vertexType == GL_FLOAT
			&& attribs.colorType == GL_FLOAT
			&& attribs.vertexComponents == 3
			&& attribs.colorComponents == 4
		) {
			addedPVRCommands = drawTriStripV3F_C4F(&attribs, count, dest);
		}

		else if(mode == GL_TRIANGLES
			&& vao->enabledAttribs == (ENABLEDVERTEXATTRIB_VERTEX | ENABLEDVERTEXATTRIB_TEXCOORD0)
			&& attribs.vertexType == GL_FLOAT
			&& attribs.texCoordType == GL_FLOAT
			&& attribs.vertexComponents == 3
			&& attribs.texCoordComponents == 2
		) {
			addedPVRCommands = drawTrisV3F_T2F(&attribs, count, dest);
		}

		// 2D
		else if(mode == GL_TRIANGLE_STRIP
			&& vao->enabledAttribs == (ENABLEDVERTEXATTRIB_VERTEX | ENABLEDVERTEXATTRIB_TEXCOORD0)
			&& attribs.vertexType == GL_FLOAT
			&& attribs.texCoordType == GL_FLOAT
			&& attribs.vertexComponents == 2
			&& attribs.texCoordComponents == 2
		) {
			addedPVRCommands = drawTriStripV2F_T2F(&attribs, count, dest);
		}
		else if(mode == GL_TRIANGLE_STRIP
			&& vao->enabledAttribs == ENABLEDVERTEXATTRIB_VERTEX
			&& attribs.vertexType == GL_FLOAT
			&& attribs.vertexComponents == 2
		) {
			addedPVRCommands = drawTriStripV2F(&attribs, count, dest);
		}

		/// \todo - Implement more drawing mode and attribute combinations.
		else {
#ifndef NDEBUG
			glSetError(GL_KOS_UNIMPLEMENTED);
#endif
			assert(0);
		}

		assert(addedPVRCommands);

		dest += addedPVRCommands;
		*listVertCount += addedPVRCommands;
	}
}

void GLAPIENTRY glDrawElements(GLenum mode, GLsizei count, GLenum type, GLvoid const *indices) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_VALUE, count < 0);

	/// \todo GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to an
	/// enabled array and the buffer object's data store is currently mapped.


	glMultiDrawElements(mode, &count, type, &indices, 1);
}

void GLAPIENTRY glDrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, GLvoid const *indices) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_VALUE, count < 0);
	GL_CHECK(GL_INVALID_VALUE, end < start);

	/// \todo GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to an
	/// enabled array and the buffer object's data store is currently mapped.


	/// \todo Check whether any performance can be gained here
	glDrawElements(mode, count, type, indices);
}

void APIENTRY glMultiDrawElements(GLenum mode, GLsizei const *count, GLenum type, void const * const *indices, GLsizei drawcount) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_VALUE, count < 0);

	/// \todo GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to an
	/// enabled array and the buffer object's data store is currently mapped.


	/// \todo Implement this function
#ifndef NDEBUG
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif
}

void GLAPIENTRY glInterleavedArrays(GLenum format, GLsizei stride, GLvoid const *pointer) {
#ifndef NDEBUG
	/// \todo Implement this function
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif
}

void GLAPIENTRY glDepthFunc(GLenum func) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidDepthFunc(func));

	setDepthMode(isDepthTestingEnabled, func);
}

void GLAPIENTRY glShadeModel(GLenum model) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, model != GL_FLAT && model != GL_SMOOTH);

	if(shadeModel != model) {
		unsigned const mode = glShadeModelToPVR(model) << PVR_TA_CMD_SHADE_SHIFT;
		for(int i = 0; i < PVR_LIST_COUNT; ++i) {
			polyHeaders[i].cmd &= ~PVR_TA_CMD_SHADE_MASK;
			polyHeaders[i].cmd |= mode;
		}

		shadeModel = model;
	}
}

GLboolean GLAPIENTRY glIsEnabled(GLenum cap) {
#ifndef NDEBUG
	if(!isValidCapability(cap)) {
		glSetError(GL_INVALID_ENUM);
		return GL_FALSE;
	}

	if(isImmediateModeActive()) {
		glSetError(GL_INVALID_OPERATION);
		return GL_FALSE;
	}
#endif


	switch(cap) {
		case GL_CULL_FACE:  return isCullingEnabled      ? GL_TRUE : GL_FALSE;
		case GL_DEPTH_TEST: return isDepthTestingEnabled ? GL_TRUE : GL_FALSE;
	}

#ifndef NDEBUG
	/// \todo Implement more capability checks for glIsEnabled().
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif

	return GL_FALSE;
}

void GLAPIENTRY glEnable(GLenum cap) {
	setCapabilityState(cap, 1);
}

void GLAPIENTRY glDisable(GLenum cap) {
	setCapabilityState(cap, 0);
}

void GLAPIENTRY glCullFace(GLenum mode) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, mode != GL_FRONT && mode != GL_BACK && mode != GL_FRONT_AND_BACK);

	setCullMode(isCullingEnabled, mode, frontFace);
}

void GLAPIENTRY glFrontFace(GLenum mode) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, mode != GL_CW && mode != GL_CCW);

	setCullMode(isCullingEnabled, cullFace, mode);
}

void GLAPIENTRY glHint(GLenum target, GLenum mode) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidHintTarget(target));
	GL_CHECK(GL_INVALID_ENUM, mode != GL_FASTEST && mode != GL_NICEST && mode != GL_DONT_CARE);
}

GLubyte const * GLAPIENTRY glGetString(GLenum name) {
#ifndef NDEBUG
	if(isImmediateModeActive()) {
		glSetError(GL_INVALID_OPERATION);
		return NULL;
	}

	if(!isValidStringName(name)) {
		glSetError(GL_INVALID_ENUM);
		return NULL;
	}
#endif


	switch(name) {
		case GL_VENDOR:     return vendorString;
		case GL_RENDERER:   return rendererString;
		case GL_VERSION:    return versionString;
		case GL_EXTENSIONS: return extensionString;
	}

	assert(0);
	return NULL;
}

void GLAPIENTRY glClearDepth(GLclampd depth) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	glSetError(GL_KOS_UNSUPPORTED);

	/// \fixme Research how this might be implemented.
}

void GLAPIENTRY glBlendFunc(GLenum sfactor, GLenum dfactor) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidBlendSourceFactor(sfactor) || !isValidBlendDestFactor(dfactor));

	blendSourceFactor = sfactor;
	blendDestFactor   = dfactor;
}
