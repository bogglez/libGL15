/**
 * @file draw.h
 * OpenGL drawing related functions.
 */

#pragma once

#include <dc/pvr.h>
#include "texture.h"

#define PVR_LIST_COUNT 5
extern pvr_poly_hdr_t polyHeaders[PVR_LIST_COUNT];

int isValidDrawMode(GLenum mode);

/**
 * Define the texture to be used from now on, when texturing is enabled
 */
void useTexture(Texture * texture);
