/**
 * @file vao.c
 * OpenGL vertex array object functions and state.
 * VAOs are not part of OpenGL 1.5 core, but are a useful extension.
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "GL/gl.h"
#include "bufferObject.h"
#include "draw.h"
#include "error.h"
#include "immediate.h"
#include "texture.h"
#include "vao.h"


/*
 * State
 */
static GLuint activeVertexArrayObjectID = 0;
static VertexArrayObject* activeVertexArrayObject = NULL;
size_t vertexArrayObjectCount = 0;
static VertexArrayObject* vertexArrayObjects = NULL;


/*
 * Internal functions
 */
static size_t getTypeSize(GLenum type) {
	switch(type) {
		case GL_BYTE:
		case GL_UNSIGNED_BYTE:
			return 1;

		case GL_SHORT:
		case GL_UNSIGNED_SHORT:
			return 2;

		case GL_INT:
		case GL_UNSIGNED_INT:
		case GL_FLOAT:
			return 4;

		case GL_DOUBLE:
			return 8;
	}

	assert(0);
	return 0;
}

static void initVertexArrayObject(VertexArrayObject * vertexArrayObject) {
	assert(vertexArrayObject);

	memset(vertexArrayObject, 0, sizeof(VertexArrayObject));

	VertexAttribs * const attribs = &vertexArrayObject->attribs;

	attribs->vertexComponents   = 4;
	attribs->texCoordComponents = 4;
	attribs->colorComponents    = 4;

	attribs->vertexType         = GL_FLOAT;
	attribs->normalType         = GL_FLOAT;
	attribs->colorType          = GL_FLOAT;
	attribs->secondaryColorType = GL_FLOAT;
	attribs->colorIndexType     = GL_FLOAT;
	attribs->fogCoordType       = GL_FLOAT;
}

static VertexArrayObject * getVertexArrayObjectByID(GLuint array) {
	if(array < vertexArrayObjectCount) {
		return &vertexArrayObjects[array];
	}

	return NULL;
}

static void destroyVertexArrayObjectByID(GLuint array) {
	assert(array > 0 && array < vertexArrayObjectCount);
	assert(glIsVertexArray(array));

	// Unbind if deleting active VAO
	if(activeVertexArrayObjectID == array) {
		glBindVertexArray(0);
	}

	VertexArrayObject *vertexArrayObject = getVertexArrayObjectByID(array);
	assert(vertexArrayObject);

	vertexArrayObject->isInitialized = 0;

	initVertexArrayObject(vertexArrayObject);
}

#ifndef NDEBUG
static int isValidClientStateCap(GLenum cap) {
	return cap == GL_COLOR_ARRAY
		|| cap == GL_EDGE_FLAG_ARRAY
		|| cap == GL_FOG_COORD_ARRAY
		|| cap == GL_INDEX_ARRAY
		|| cap == GL_NORMAL_ARRAY
		|| cap == GL_SECONDARY_COLOR_ARRAY
		|| cap == GL_TEXTURE_COORD_ARRAY
		|| cap == GL_VERTEX_ARRAY;
}

static int isValidVertexType(GLenum type) {
	return type == GL_SHORT
		|| type == GL_INT
		|| type == GL_FLOAT
		|| type == GL_DOUBLE;
}

static int isValidNormalType(GLenum type) {
	return type == GL_BYTE
		|| type == GL_SHORT
		|| type == GL_INT
		|| type == GL_FLOAT
		|| type == GL_DOUBLE;
}

static int isValidTexCoordType(GLenum type) {
	return type == GL_SHORT
		|| type == GL_INT
		|| type == GL_FLOAT
		|| type == GL_DOUBLE;
}

static int isValidColorType(GLenum type) {
	return type == GL_BYTE
		|| type == GL_UNSIGNED_BYTE
		|| type == GL_SHORT
		|| type == GL_UNSIGNED_SHORT
		|| type == GL_INT
		|| type == GL_UNSIGNED_INT
		|| type == GL_FLOAT
		|| type == GL_DOUBLE;
}

static int isValidColorIndexType(GLenum type) {
	return type == GL_UNSIGNED_BYTE
		|| type == GL_SHORT
		|| type == GL_INT
		|| type == GL_FLOAT
		|| type == GL_DOUBLE;
}

static int isValidFogCoordType(GLenum type) {
	return type == GL_FLOAT
		|| type == GL_DOUBLE;
}
#endif


/*
 * Functions
 */
int initVertexArrayObjects(size_t requestedVertexArrayObjectCount) {
	assert(!vertexArrayObjects);

	vertexArrayObjects = malloc(requestedVertexArrayObjectCount * sizeof(VertexArrayObject));
	if(!vertexArrayObjects) {
		return -1;
	}

	activeVertexArrayObject = &vertexArrayObjects[activeVertexArrayObjectID];

	vertexArrayObjectCount = requestedVertexArrayObjectCount;
	for(size_t i = 0; i < vertexArrayObjectCount; ++i) {
		initVertexArrayObject(&vertexArrayObjects[i]);
	}

	// VAO 0 is initialized by default
	getVertexArrayObjectByID(0)->isInitialized = 1;

	return 0;
}

VertexArrayObject * getActiveVertexArrayObject() {
	assert(activeVertexArrayObject && activeVertexArrayObjectID < vertexArrayObjectCount);

	return activeVertexArrayObject;
}


/*
 * OpenGL functions
 */
void GLAPIENTRY glEnableClientState(GLenum cap) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidClientStateCap(cap));

	assert(activeVertexArrayObject);


	switch(cap) {
		case GL_VERTEX_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_VERTEX;
			break;

		case GL_NORMAL_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_NORMAL;
			break;

		// Enables currently active texture unit
		case GL_TEXTURE_COORD_ARRAY:
			activeVertexArrayObject->enabledAttribs |= (ENABLEDVERTEXATTRIB_TEXCOORD0 << getActiveTextureUnitNumber());
			break;

		case GL_COLOR_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_COLOR;
			break;

		case GL_SECONDARY_COLOR_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_SECONDARY_COLOR;
			break;

		case GL_INDEX_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_COLORINDEX;
			break;

		case GL_EDGE_FLAG_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_EDGE;
			break;

		case GL_FOG_COORDINATE_ARRAY:
			activeVertexArrayObject->enabledAttribs |= ENABLEDVERTEXATTRIB_FOGCOORD;
			break;

		default:
#ifndef NDEBUG
			/// \todo Implement more client states.
			glSetError(GL_KOS_UNIMPLEMENTED);
#endif
			return;
	}
}

void GLAPIENTRY glDisableClientState(GLenum cap) {
#ifndef NDEBUG
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	GL_CHECK(GL_INVALID_ENUM, !isValidClientStateCap(cap));
#endif

	assert(activeVertexArrayObject);


	switch(cap) {
		case GL_VERTEX_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_VERTEX;
			break;

		case GL_NORMAL_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_NORMAL;
			break;

		case GL_TEXTURE_COORD_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~(ENABLEDVERTEXATTRIB_TEXCOORD0 << getActiveTextureUnitNumber());
			break;

		case GL_COLOR_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_COLOR;
			break;

		case GL_SECONDARY_COLOR_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_SECONDARY_COLOR;
			break;

		case GL_INDEX_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_COLORINDEX;
			break;

		case GL_EDGE_FLAG_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_EDGE;
			break;

		case GL_FOG_COORDINATE_ARRAY:
			activeVertexArrayObject->enabledAttribs &= ~ENABLEDVERTEXATTRIB_FOGCOORD;
			break;

		default:
#ifndef NDEBUG
			/// \todo Implement more client states.
			glSetError(GL_KOS_UNIMPLEMENTED);
#endif
			return;
	}
}

void GLAPIENTRY glVertexPointer(GLint size, GLenum type, GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, size < 2 || size > 4);
	GL_CHECK(GL_INVALID_ENUM, !isValidVertexType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->vertexBinding = vboID;
	attribs->vertex           = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->vertexComponents = size;
	attribs->vertexType       = type;
	attribs->vertexStride     = (unsigned)stride ? (unsigned)stride : (unsigned)size * getTypeSize(type);
}

void GLAPIENTRY glNormalPointer(GLenum type, GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidNormalType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->normalBinding = vboID;
	attribs->normal        = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->normalType    = type;
	attribs->normalStride  = (unsigned)stride ? (unsigned)stride : 3 * getTypeSize(type);
}

void GLAPIENTRY glTexCoordPointer(GLint size, GLenum type, GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, size < 1 || size > 4);
	GL_CHECK(GL_INVALID_ENUM, !isValidTexCoordType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->texCoordBinding = vboID;
	attribs->texCoord           = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->texCoordComponents = size;
	attribs->texCoordType       = type;
	attribs->texCoordStride     = (unsigned)stride ? (unsigned)stride : (unsigned)size * getTypeSize(type);
}

void GLAPIENTRY glColorPointer(GLint size, GLenum type, GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, size < 3 || size > 4);
	GL_CHECK(GL_INVALID_ENUM, !isValidColorType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->colorBinding = vboID;
	attribs->color           = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->colorComponents = size;
	attribs->colorType       = type;
	attribs->colorStride     = (unsigned)stride ? (unsigned)stride : (unsigned)size * getTypeSize(type);
}

void APIENTRY glSecondaryColorPointer(GLint size, GLenum type, GLsizei stride, void const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, size != 3);
	GL_CHECK(GL_INVALID_ENUM, !isValidColorType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->secondaryColorBinding = vboID;
	attribs->secondaryColor        = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->secondaryColorType    = type;
	attribs->secondaryColorStride  = (unsigned)stride ? (unsigned)stride : (unsigned)size * getTypeSize(type);
}

void GLAPIENTRY glIndexPointer(GLenum type, GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidColorIndexType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->colorIndexBinding = vboID;
	attribs->colorIndex        = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->colorIndexType    = type;
	attribs->colorIndexStride  = (unsigned)stride ? (unsigned)stride : getTypeSize(type);
}

void GLAPIENTRY glEdgeFlagPointer(GLsizei stride, GLvoid const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->edgeFlagBinding = vboID;
	attribs->edgeFlag        = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->edgeFlagStride  = (unsigned)stride ? (unsigned)stride : getTypeSize(GL_BYTE);
}

void APIENTRY glFogCoordPointer(GLenum type, GLsizei stride, void const *ptr) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidFogCoordType(type));
	GL_CHECK(GL_INVALID_VALUE, stride < 0);


	GLsizei const vboID = getActiveBufferObjectIDForTarget(GL_ARRAY_BUFFER);
	char const * const vboData = getBufferObjectByID(vboID)->data;
	VertexAttribs * const attribs = &activeVertexArrayObject->attribs;

	activeVertexArrayObject->fogCoordBinding = vboID;
	attribs->fogCoord        = vboID ? vboData + (ptrdiff_t)ptr : ptr;
	attribs->fogCoordType    = type;
	attribs->fogCoordStride  = (unsigned)stride ? (unsigned)stride : getTypeSize(type);
}

void APIENTRY glBindVertexArray(GLuint array) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_OPERATION, array && !glIsVertexArray(array));


	activeVertexArrayObjectID = array;
	activeVertexArrayObject = &vertexArrayObjects[array];
}

void APIENTRY glDeleteVertexArrays(GLsizei n, GLuint const *arrays) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	// Revert binding if buffer to delete is currently bound
	if(activeVertexArrayObjectID) {
		for(GLsizei i = 0; i < n; ++i) {
			if(activeVertexArrayObjectID == arrays[i]) {
				glBindVertexArray(0);
				break;
			}
		}
	}

	for(GLsizei i = 0; i < n; ++i) {
		GLuint const array = arrays[i];
		VertexArrayObject *vertexArrayObject = getVertexArrayObjectByID(array);

		// glDeleteBuffers() ignores invalid IDs
		if(!array || !vertexArrayObject) {
			continue;
		}

		destroyVertexArrayObjectByID(array);
	}
}

void APIENTRY glGenVertexArrays(GLsizei n, GLuint *arrays) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	unsigned i;
	for(i = 1; i <= (unsigned)n && i < vertexArrayObjectCount; ++i) {
		if(!glIsVertexArray(i)) {
			arrays[i-1] = i;
			getVertexArrayObjectByID(i)->isInitialized = 1;
		}
	}

#ifndef NDEBUG
	if(i <= (unsigned)n) {
		glSetError(GL_OUT_OF_MEMORY); // not standard but makes sense
		return;
	}
#endif
}

GLboolean APIENTRY glIsVertexArray(GLuint array) {
	if(array > 0
		&& array < vertexArrayObjectCount
		&& vertexArrayObjects[array].isInitialized) {
		return GL_TRUE;
	}

	return GL_FALSE;
}
