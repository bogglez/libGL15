/**
 * @file immediate.c
 * OpenGL immediate drawing mode related functions.
 */

#include <assert.h>
#include "GL/gl.h"
#include "draw.h"
#include "error.h"


/*
 * State
 */
#ifndef NDEBUG
static int isImmediateMode = 0;
#endif


/*
 * Functions
 */
#ifndef NDEBUG
int isImmediateModeActive() {
	return isImmediateMode;
}
#endif


/*
 * OpenGL functions
 */
void GLAPIENTRY glBegin(GLenum mode) {
	GL_CHECK(GL_INVALID_ENUM, !isValidDrawMode(mode));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


#ifndef NDEBUG
	// No need to track immediate mode state with error reporting off
	isImmediateMode = 1;
#endif
}

void GLAPIENTRY glEnd() {
	GL_CHECK(GL_INVALID_OPERATION, !isImmediateModeActive());


#ifndef NDEBUG
	// No need to track immediate mode state with error reporting off
	isImmediateMode = 0;
#endif
}


// Vertex
void GLAPIENTRY glVertex3f(GLfloat x, GLfloat y, GLfloat z) {
	assert(isImmediateModeActive()); // Undefined behavior
}

void GLAPIENTRY glVertex3d(GLdouble x, GLdouble y, GLdouble z) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3f(x, y, z);
}

void GLAPIENTRY glVertex3i(GLint x, GLint y, GLint z) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3f(x, y, z);
}

void GLAPIENTRY glVertex3s(GLshort x, GLshort y, GLshort z) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3i(x, y, z);
}

void GLAPIENTRY glVertex2f(GLfloat x, GLfloat y) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3f(x, y, 0.f);
}

void GLAPIENTRY glVertex2d(GLdouble x, GLdouble y) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3d(x, y, 0.);
}

void GLAPIENTRY glVertex2i(GLint x, GLint y) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2f(x, y);
}

void GLAPIENTRY glVertex2s(GLshort x, GLshort y) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2i(x, y);
}


void GLAPIENTRY glVertex3dv(GLdouble const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3d(v[0], v[1], v[2]);
}

void GLAPIENTRY glVertex3fv(GLfloat const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3f(v[0], v[1], v[2]);
}

void GLAPIENTRY glVertex3iv(GLint const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3i(v[0], v[1], v[2]);
}

void GLAPIENTRY glVertex3sv(GLshort const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex3s(v[0], v[1], v[2]);
}

void GLAPIENTRY glVertex2dv(GLdouble const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2d(v[0], v[1]);
}

void GLAPIENTRY glVertex2fv(GLfloat const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2f(v[0], v[1]);
}

void GLAPIENTRY glVertex2iv(GLint const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2i(v[0], v[1]);
}

void GLAPIENTRY glVertex2sv(GLshort const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glVertex2s(v[0], v[1]);
}


// Texture
void GLAPIENTRY glTexCoord3f(GLfloat s, GLfloat t, GLfloat u) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord3f(s, t, u);
}

void GLAPIENTRY glTexCoord3d(GLdouble s, GLdouble t, GLdouble u) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord3f(s, t, u);
}

void GLAPIENTRY glTexCoord2f(GLfloat s, GLfloat t) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord3f(s, t, 0.f);
}

void GLAPIENTRY glTexCoord2d(GLdouble s, GLdouble t) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord3d(s, t, 0.);
}

void GLAPIENTRY glTexCoord2i(GLint s, GLint t) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2f(s, t);
}

void GLAPIENTRY glTexCoord2s(GLshort s, GLshort t) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2i(s, t);
}

void GLAPIENTRY glTexCoord1f(GLfloat s) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2f(s, 0.f);
}

void GLAPIENTRY glTexCoord1d(GLdouble s) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2d(s, 0.);
}

void GLAPIENTRY glTexCoord1i(GLint s) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord1f(s);
}

void GLAPIENTRY glTexCoord1s(GLshort s) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord1f(s);
}


void GLAPIENTRY glTexCoord2dv(GLdouble const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2d(v[0], v[1]);
}

void GLAPIENTRY glTexCoord2fv(GLfloat const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2f(v[0], v[1]);
}

void GLAPIENTRY glTexCoord2iv(GLint const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2i(v[0], v[1]);
}

void GLAPIENTRY glTexCoord2sv(GLshort const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord2s(v[0], v[1]);
}


void GLAPIENTRY glTexCoord1dv(GLdouble const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord1d(v[0]);
}

void GLAPIENTRY glTexCoord1iv(GLint const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord1i(v[0]);
}

void GLAPIENTRY glTexCoord1sv(GLshort const *v) {
	assert(isImmediateModeActive()); // Undefined behavior
	glTexCoord1s(v[0]);
}
