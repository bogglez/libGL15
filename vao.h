/**
 * @file vao.h
 * OpenGL vertex array object related functions.
 */

#pragma once

#include "GL/gl.h"


/**
 * Keeps track of enabled attributes in VAO.
 * This information will change what data draw calls will process.
 */
typedef enum {
	ENABLEDVERTEXATTRIB_NONE            = 1 << 0,
	ENABLEDVERTEXATTRIB_VERTEX          = 1 << 1,
	ENABLEDVERTEXATTRIB_NORMAL          = 1 << 2,
	ENABLEDVERTEXATTRIB_COLOR           = 1 << 3,
	ENABLEDVERTEXATTRIB_SECONDARY_COLOR = 1 << 4,
	ENABLEDVERTEXATTRIB_COLORINDEX      = 1 << 5,
	ENABLEDVERTEXATTRIB_EDGE            = 1 << 6,
	ENABLEDVERTEXATTRIB_FOGCOORD        = 1 << 7,

	// Multiple texcoords may be active for multi-texturing
	ENABLEDVERTEXATTRIB_TEXCOORD0       = 1 << 8,
	ENABLEDVERTEXATTRIB_TEXCOORD1       = 1 << 9,
	ENABLEDVERTEXATTRIB_TEXCOORD2       = 1 << 10,
	ENABLEDVERTEXATTRIB_TEXCOORD_MASK   = (ENABLEDVERTEXATTRIB_TEXCOORD0 | ENABLEDVERTEXATTRIB_TEXCOORD1 | ENABLEDVERTEXATTRIB_TEXCOORD2),
} EnabledVertexAttribMask;

typedef struct {
	unsigned char vertexComponents;
	unsigned char texCoordComponents;
	unsigned char colorComponents;
	unsigned char padding;

	GLenum vertexType;
	GLenum normalType;
	GLenum texCoordType;
	GLenum colorType;
	GLenum secondaryColorType;
	GLenum colorIndexType;
	GLenum fogCoordType;

	void const *vertex;
	void const *normal;
	void const *texCoord;
	void const *color;
	void const *secondaryColor;
	void const *colorIndex;
	void const *edgeFlag;
	void const *fogCoord;

	unsigned short vertexStride;
	unsigned short normalStride;
	unsigned short texCoordStride;
	unsigned short colorStride;
	unsigned short secondaryColorStride;
	unsigned short colorIndexStride;
	unsigned short edgeFlagStride;
	unsigned short fogCoordStride;
} VertexAttribs;

/**
 * State of a VAO.
 * Tracked state includes
 * - enabled attributes
 * - the bound buffer object for each attribute
 * - amount of components for each attribute
 * - initialization state
 * - stride of each attribute's input data
 * - data type of each attribute
 * - data pointers for each attribute
 */
typedef struct {
	int isInitialized;

	// Buffer object at the time of gl*Pointer()
	GLuint vertexBinding;
	GLuint normalBinding;
	GLuint texCoordBinding;
	GLuint colorBinding;
	GLuint secondaryColorBinding;
	GLuint colorIndexBinding;
	GLuint edgeFlagBinding;
	GLuint fogCoordBinding;
	GLuint elementArrayBufferID;

	EnabledVertexAttribMask enabledAttribs;
	VertexAttribs attribs;
} VertexArrayObject;


/*
 * Functions
 */

/**
 * Initializes all VAO state.
 * Called during context creation.
 * @return 0 on success, non-zero on failure.
 */
int initVertexArrayObjects(size_t requestedVertexArrayObjectCount);

/**
 * Returns the currently active VAO's state.
 * @return Currently active VAO's state struct.
 */
VertexArrayObject * getActiveVertexArrayObject();
