/**
 * @file error.c
 * OpenGL error reporting.
 */

#include <assert.h>
#include "error.h"

#ifndef NDEBUG

#define LOWEST_ERROR  GL_INVALID_ENUM
#define HIGHEST_ERROR GL_KOS_UNIMPLEMENTED


static char const * const errorStrings[] = {
	"GL_INVALID_ENUM",
	"GL_INVALID_VALUE",
	"GL_INVALID_OPERATION",
	"GL_STACK_OVERFLOW",
	"GL_STACK_UNDERFLOW",
	"GL_OUT_OF_MEMORY",
	"GL_KOS_UNIMPLEMENTED",
};


/*
 * State
 */
static unsigned errors = 0;


/*
 * Internal functions
 */
static int isValidError(GLenum error) {
	return error >= LOWEST_ERROR && error <= HIGHEST_ERROR;
}
#endif

/*
 * OpenGL functions
 */
// returns only one error on each call
GLenum glGetError(void) {
#ifndef NDEBUG
	// report error with lowest value first
	if(errors) {
		for(unsigned i = 0; i < 8 * sizeof(errors); ++i) {
			unsigned const mask = 1 << i;
			if(errors & mask) {
				errors &= ~mask; // clear this error
				return LOWEST_ERROR + i;
			}
		}

		assert(0);
	}
#endif

	return GL_NO_ERROR;
}

#ifndef NDEBUG
void glSetError(GLenum flag) {
	assert(isValidError(flag));
	if(flag != GL_NO_ERROR) {
		errors |= 1 << (flag - LOWEST_ERROR);
	}
}
#endif
