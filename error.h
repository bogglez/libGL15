/**
 * @file error.h
 * OpenGL error related functions.
 */

#pragma once

#include "GL/gl.h"

#ifndef NDEBUG
	#define GL_CHECK(errorCode, cond) { if(cond) { glSetError(errorCode); return; } }
#else
	#define GL_CHECK(errorCode, cond)
#endif

/*
 * Functions
 */

/**
 * Sets an OpenGL error flag for use with glGetError().
 */
void glSetError(GLenum flag);
