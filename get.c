#include <assert.h>
#include "GL/gl.h"
#include "error.h"
#include "immediate.h"
#include "matrix.h"
#include "util.h"

/*
 * Internal functions
 */
#ifndef NDEBUG
/**
 * Checks whether a parameter name may be used with glGet* functions.
 * \param pname The parameter name to be checked for validity.
 * \param type  The type of glGet*: GL_DOUBLE, GL_FLOAT, GL_INT, GL_BYTE (for bool).
 * \fixme This currently does not check whether a pname makes sense with the given type.
 */
static int isValidGetParam(GLenum const pname) {
	return
	    pname == GL_COLOR_LOGIC_OP
	 || pname == GL_COLOR_ARRAY
	 || pname == GL_COLOR_ARRAY_SIZE
	 || pname == GL_COLOR_ARRAY_STRIDE
	 || pname == GL_COLOR_ARRAY_TYPE
	 || pname == GL_EDGE_FLAG_ARRAY
	 || pname == GL_EDGE_FLAG_ARRAY_STRIDE
	 || pname == GL_INDEX_ARRAY
	 || pname == GL_INDEX_ARRAY_STRIDE
	 || pname == GL_INDEX_ARRAY_TYPE
	 || pname == GL_INDEX_LOGIC_OP
	 || pname == GL_NORMAL_ARRAY
	 || pname == GL_NORMAL_ARRAY_STRIDE
	 || pname == GL_NORMAL_ARRAY_TYPE
	 || pname == GL_POLYGON_OFFSET_UNITS
	 || pname == GL_POLYGON_OFFSET_FACTOR
	 || pname == GL_POLYGON_OFFSET_FILL
	 || pname == GL_POLYGON_OFFSET_LINE
	 || pname == GL_POLYGON_OFFSET_POINT
	 || pname == GL_TEXTURE_COORD_ARRAY
	 || pname == GL_TEXTURE_COORD_ARRAY_SIZE
	 || pname == GL_TEXTURE_COORD_ARRAY_STRIDE
	 || pname == GL_TEXTURE_COORD_ARRAY_TYPE
	 || pname == GL_VERTEX_ARRAY
	 || pname == GL_VERTEX_ARRAY_SIZE
	 || pname == GL_VERTEX_ARRAY_STRIDE
	 || pname == GL_VERTEX_ARRAY_TYPE
	 || pname == GL_ALIASED_POINT_SIZE_RANGE
	 || pname == GL_FEEDBACK_BUFFER_SIZE
	 || pname == GL_FEEDBACK_BUFFER_TYPE
	 || pname == GL_LIGHT_MODEL_AMBIENT
	 || pname == GL_LIGHT_MODEL_COLOR_CONTROL
	 || pname == GL_MAX_3D_TEXTURE_SIZE
	 || pname == GL_MAX_ELEMENTS_INDICES
	 || pname == GL_MAX_ELEMENTS_VERTICES
	 || pname == GL_PACK_IMAGE_HEIGHT
	 || pname == GL_PACK_SKIP_IMAGES
	 || pname == GL_RESCALE_NORMAL
	 || pname == GL_SELECTION_BUFFER_SIZE
	 || pname == GL_SMOOTH_LINE_WIDTH_GRANULARITY
	 || pname == GL_SMOOTH_LINE_WIDTH_RANGE
	 || pname == GL_SMOOTH_POINT_SIZE_GRANULARITY
	 || pname == GL_SMOOTH_POINT_SIZE_RANGE
	 || pname == GL_TEXTURE_3D
	 || pname == GL_TEXTURE_BINDING_3D
	 || pname == GL_UNPACK_IMAGE_HEIGHT
	 || pname == GL_UNPACK_SKIP_IMAGES
	 || pname == GL_COMPRESSED_TEXTURE_FORMATS
	 || pname == GL_NUM_COMPRESSED_TEXTURE_FORMATS
	 || pname == GL_TEXTURE_BINDING_CUBE_MAP
	 || pname == GL_TEXTURE_COMPRESSION_HINT
	 || pname == GL_BLEND_DST_ALPHA
	 || pname == GL_BLEND_DST_RGB
	 || pname == GL_BLEND_SRC_ALPHA
	 || pname == GL_BLEND_SRC_RGB
	 || pname == GL_CURRENT_FOG_COORD
	 || pname == GL_CURRENT_SECONDARY_COLOR
	 || pname == GL_FOG_COORD_ARRAY_STRIDE
	 || pname == GL_FOG_COORD_ARRAY_TYPE
	 || pname == GL_FOG_COORD_SRC
	 || pname == GL_MAX_TEXTURE_LOD_BIAS
	 || pname == GL_POINT_SIZE_MIN
	 || pname == GL_POINT_SIZE_MAX
	 || pname == GL_POINT_FADE_THRESHOLD_SIZE
	 || pname == GL_POINT_DISTANCE_ATTENUATION
	 || pname == GL_SECONDARY_COLOR_ARRAY_SIZE
	 || pname == GL_SECONDARY_COLOR_ARRAY_STRIDE
	 || pname == GL_SECONDARY_COLOR_ARRAY_TYPE
	 || pname == GL_ARRAY_BUFFER_BINDING
	 || pname == GL_COLOR_ARRAY_BUFFER_BINDING
	 || pname == GL_EDGE_FLAG_ARRAY_BUFFER_BINDING
	 || pname == GL_ELEMENT_ARRAY_BUFFER_BINDING
	 || pname == GL_FOG_COORD_ARRAY_BUFFER_BINDING
	 || pname == GL_INDEX_ARRAY_BUFFER_BINDING
	 || pname == GL_NORMAL_ARRAY_BUFFER_BINDING
	 || pname == GL_SECONDARY_COLOR_ARRAY_BUFFER_BINDING
	 || pname == GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING
	 || pname == GL_VERTEX_ARRAY_BUFFER_BINDING
	 || pname == GL_COLOR_MATRIX
	 || pname == GL_COLOR_MATRIX_STACK_DEPTH
	 || pname == GL_COLOR_TABLE
	 || pname == GL_CONVOLUTION_1D
	 || pname == GL_CONVOLUTION_2D
	 || pname == GL_HISTOGRAM
	 || pname == GL_MAX_COLOR_MATRIX_STACK_DEPTH
	 || pname == GL_MINMAX
	 || pname == GL_POST_COLOR_MATRIX_COLOR_TABLE
	 || pname == GL_POST_COLOR_MATRIX_RED_BIAS
	 || pname == GL_POST_COLOR_MATRIX_GREEN_BIAS
	 || pname == GL_POST_COLOR_MATRIX_BLUE_BIAS
	 || pname == GL_POST_COLOR_MATRIX_ALPHA_BIAS
	 || pname == GL_POST_COLOR_MATRIX_RED_SCALE
	 || pname == GL_POST_COLOR_MATRIX_GREEN_SCALE
	 || pname == GL_POST_COLOR_MATRIX_BLUE_SCALE
	 || pname == GL_POST_COLOR_MATRIX_ALPHA_SCALE
	 || pname == GL_POST_CONVOLUTION_COLOR_TABLE
	 || pname == GL_POST_CONVOLUTION_RED_BIAS
	 || pname == GL_POST_CONVOLUTION_GREEN_BIAS
	 || pname == GL_POST_CONVOLUTION_BLUE_BIAS
	 || pname == GL_POST_CONVOLUTION_ALPHA_BIAS
	 || pname == GL_POST_CONVOLUTION_RED_SCALE
	 || pname == GL_POST_CONVOLUTION_GREEN_SCALE
	 || pname == GL_POST_CONVOLUTION_BLUE_SCALE
	 || pname == GL_POST_CONVOLUTION_ALPHA_SCALE
	 || pname == GL_SEPARABLE_2D
	 || pname == GL_CURRENT_RASTER_TEXTURE_COORDS
	 || pname == GL_TEXTURE_1D
	 || pname == GL_TEXTURE_BINDING_1D
	 || pname == GL_TEXTURE_2D
	 || pname == GL_TEXTURE_BINDING_2D
	 || pname == GL_TEXTURE_3D
	 || pname == GL_TEXTURE_BINDING_3D
	 || pname == GL_TEXTURE_GEN_S
	 || pname == GL_TEXTURE_GEN_T
	 || pname == GL_TEXTURE_GEN_R
	 || pname == GL_TEXTURE_GEN_Q
	 || pname == GL_TEXTURE_MATRIX
	 || pname == GL_TEXTURE_STACK_DEPTH
	 || pname == GL_TEXTURE_COORD_ARRAY
	 || pname == GL_TEXTURE_COORD_ARRAY_BUFFER_BINDING
	 || pname == GL_TEXTURE_COORD_ARRAY_SIZE
	 || pname == GL_TEXTURE_COORD_ARRAY_STRIDE
	 || pname == GL_TEXTURE_COORD_ARRAY_TYPE;
}
#endif

/**
 * Set values for glGet* depending on type. Casts floats/doubles to 1/0.
 * Do not use this for color values, they're mapped to the integer range instead.
 */
static void setGenericInt(void *params, GLint value, GLenum type ) {
	assert(type == GL_FLOAT || type == GL_BYTE || type == GL_DOUBLE || type == GL_INT);

	if(type == GL_FLOAT) {
		*(GLfloat*)params = (GLfloat)!!value;
	}
	else if(type == GL_DOUBLE) {
		*(GLdouble*)params = (GLdouble)!!value;
	}
	else if(type == GL_BYTE) {
		*(GLboolean*)params = (GLboolean)!!value;
	}
	else if(type == GL_INT) {
		*(GLint*)params = (GLint)value;
	}
}

static void getGeneric(GLenum pname, void *params, GLenum type) {
	assert(type == GL_FLOAT || type == GL_BYTE || type == GL_DOUBLE || type == GL_INT);

	/// \fixme Add missing pnames
	switch(pname) {
	case GL_COLOR_MATRIX:
	case GL_MODELVIEW_MATRIX:
	case GL_PROJECTION_MATRIX:
	case GL_TEXTURE_MATRIX:
		if(type == GL_FLOAT) {
			copyMatrixf(pname, params);
		}
		else {
			float m[16];
			copyMatrixf(pname, m);
			if(type == GL_DOUBLE) {
				for(unsigned i = 0; i < 16; ++i) {
					((GLdouble*)params)[i] = m[i];
				}
			}
			else if(type == GL_INT) {
				for(unsigned i = 0; i < 16; ++i) {
					((GLint*)params)[i] = m[i] != 0.f ? GL_TRUE : GL_FALSE;
				}
			}
			else if(type == GL_BYTE) {
				for(unsigned i = 0; i < 16; ++i) {
					((GLboolean*)params)[i] = m[i] != 0.f ? GL_TRUE : GL_FALSE;
				}
			}
		}
		break;

	case GL_SAMPLES:
		setGenericInt(params, 0, type);
		break;


	default:
		glSetError(GL_KOS_UNSUPPORTED);
		return;
	}
}

void GLAPIENTRY glGetDoublev(GLenum pname, GLdouble *params) {
	GL_CHECK(GL_INVALID_ENUM, isValidGetParam(pname));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	getGeneric(pname, params, GL_DOUBLE);
}

void GLAPIENTRY glGetFloatv(GLenum pname, GLfloat *params) {
	GL_CHECK(GL_INVALID_ENUM, isValidGetParam(pname));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	getGeneric(pname, params, GL_FLOAT);
}

void GLAPIENTRY glGetIntegerv(GLenum pname, GLint *params) {
	GL_CHECK(GL_INVALID_ENUM, isValidGetParam(pname));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	getGeneric(pname, params, GL_INT);
}

void GLAPIENTRY glGetBooleanv(GLenum pname, GLboolean *params) {
	GL_CHECK(GL_INVALID_ENUM, isValidGetParam(pname));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	getGeneric(pname, params, GL_BYTE);
}
