/**
 * @file util.h
 * Common/generic utility functions and definitions.
 */

#pragma once

/**
 * Clamps the argument to the range [MIN, MAX]
 */
#define CLAMP(X, MIN, MAX) ((X) < (MIN) ? (MIN) : ((X) > (MAX) ? (MAX) : (X)))

/**
 * Clamps the argument to the range [0, 1]
 */
#define CLAMP1(X)          CLAMP((X), 0, 1)

/**
 * Force alignment to 32 bytes.
 * Used for matrices for example so that they fit into the Dreamcast's matrix register.
 */
#define ALIGN32 __attribute__((aligned(32)))
