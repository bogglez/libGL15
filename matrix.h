/**
 * @file matrix.h
 * OpenGL matrix transformation functions.
 */

#pragma once

/**
 * Recomputes common matrix transform chains if they're dirty.
 */
void updateCachedMatrices();

/**
 * Loads the model-to-window matrix into the Dreamcast's matrix registers.
 * Used during draw calls to transform positions into window space when
 * no clipping is required.
 */
void loadModelToWindowMatrixToRegisters();

/**
 * Loads the model-to-clip matrix into the Dreamcast's matrix registers.
 * Used during draw calls to transform positions into a space where
 * clipping may be performed.
 */
void loadModelToClipMatrixToRegisters();

/**
 * Loads the NDC-to-window matrix into the Dreamcast's matrix registers.
 * Used during draw calls to transform positions after clipping was
 * performed.
 */
void loadNDCToWindowMatrixToRegisters();

void copyMatrixf(GLenum matrix, float * dest);
