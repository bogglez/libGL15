#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glu.h>


#define X0 -0.5
#define X1  0.5
#define Y0 -0.5
#define Y1  0.5
#define Z  -1

#define R 255,0,0
#define G 0,255,0
#define B 0,0,255
#define Y 255,255,0

#define TEX_WIDTH  8
#define TEX_HEIGHT 8

// The V-axis is flipped in OpenGL
unsigned char texels[TEX_WIDTH * TEX_HEIGHT * 3] = {
	R, R, R, R,  G, G, G, G,
	R, R, R, R,  G, G, G, G,
	R, R, R, R,  G, G, G, G,
	R, R, R, R,  G, G, G, G,

	B, B, B, B,  Y, Y, Y, Y,
	B, B, B, B,  Y, Y, Y, Y,
	B, B, B, B,  Y, Y, Y, Y,
	B, B, B, B,  Y, Y, Y, Y,
};

extern uint8 romdisk[];
KOS_INIT_ROMDISK(romdisk);

GLuint textureIDs[2];

/*
 * Vertex attributes
 */
GLfloat const vertexData[] = {
	/* 3D Coordinate, texture coordinate */
	X0, Y1, Z,  0, 1,
	X0, Y0, Z,  0, 0,
	X1, Y1, Z,  1, 1,
	X1, Y0, Z,  1, 0,
};

size_t const stride = 5 * sizeof(float);


/**
 * Helper function to load the raw texture data into an array.
 * @param filename The filename of the texture.
 * @param data A pointer to an array where the data should be stored.
 * @param size A pointer to a variable where to size should be stored.
 * @return 0 on success, non-zero on error.
 */
int readFileContents(char const * const filename, void **data, size_t *size) {
	assert(filename && data && size);

	FILE *f = NULL;

	f = fopen(filename, "rb");

	// Read texture from file
	if(!f) {
		return 1;
	}

	fseek(f, 0, SEEK_END);
	long imageSize = ftell(f);
	rewind(f);

	*data = malloc(imageSize);
	if(!*data) {
		fclose(f);
		return 2;
	}

	fread(*data, 1, imageSize, f);
	fclose(f);

	*size = imageSize;

	return 0;
}

int initGL() {
	glKosInit();

	glClearColor(0.1, 0.2, 0.4, 1);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer  (3, GL_FLOAT, stride, vertexData);
	glTexCoordPointer(2, GL_FLOAT, stride, vertexData + 3);

	size_t imageSize;
	void *data;
	if(readFileContents("/rd/fruit.vq", &data, &imageSize)) {
		puts("Cannot read texture file.");
		return 1;
	}

	glGenTextures(2, textureIDs);
	if(!textureIDs[0] || !textureIDs[1]) {
		puts("Cannot generate texture IDs.");
		free(data);
		return 1;
	}

	glBindTexture(GL_TEXTURE_2D, textureIDs[0]);
	glCompressedTexImage2D(GL_TEXTURE_2D, 0, GL_KOS_RGB565_TWIDDLEDVQ, 512, 512, 0, imageSize, data);
	free(data);

	glBindTexture(GL_TEXTURE_2D, textureIDs[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, TEX_WIDTH, TEX_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, texels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	return 0;
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

	glTranslatef(-0.5, 0, 0);
	glBindTexture(GL_TEXTURE_2D, textureIDs[0]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glTranslatef(1, 0, 0);
	glBindTexture(GL_TEXTURE_2D, textureIDs[1]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

int main(int argc, char **argv) {
	if(initGL()) {
		puts("Cannot init OpenGL!");
		return -1;
	}

	while(1) {
		draw();

		glSwapBuffersKOS();

		int error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	glDeleteTextures(2, textureIDs);

	return 0;
}
