#include <stdint.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>


int main(int argc, char **argv) {
	int error;

	puts("main()");

	glKosInit();

	uint8_t i = 0;

	while(1) {
		float const c = i/255.f;

		glClearColor(c, 0.2, 0.2, 1);
		glClear(GL_COLOR_BUFFER_BIT);
		glSwapBuffersKOS();
		++i;

		error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	return 0;
}
