#include <assert.h>
#include <math.h>
#include <dc/fmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


#define X0 -0.5
#define X1  0.5
#define Y0 -0.7388
#define Y1  0.7388
#define Z0  0.03
#define Z1 -0.03

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

extern uint8 romdisk[];
KOS_INIT_ROMDISK(romdisk);

GLuint vaoID;
GLuint bufferID;
GLuint textureID;

/**
 * This will store the standard matrix transformation
 * to be applied after clearing the screen.
 */
float transform[16];

/*
 * Vertex attributes will be copied into the vertex buffer object (xyz, uv).
 */
GLfloat const vertexData[34 * 5] = {
	// Front quad
	X0, Y1, Z0,  0.5263671875, 1,
	X0, Y0, Z0,  0.5263671875, 0,
	X1, Y1, Z0,  1.f, 1,
	X1, Y0, Z0,  1.f, 0,

	X1, Y0, Z0,  1.f, 0,
	X1, Y1, Z1,  0, 1,

	// Back quad
	X1, Y1, Z1,  0, 1,
	X1, Y0, Z1,  0, 0,
	X0, Y1, Z1,  0.4736328125, 1,
	X0, Y0, Z1,  0.4736328125, 0,

	X0, Y0, Z1,  0.4736328125, 0,
	X0, Y0, Z0,  0.5f, 1,

	// Bottom quad
	X0, Y0, Z0,  0.5f, 1.f,
	X0, Y0, Z1,  0.5f, 1.f,
	X1, Y0, Z0,  0.5f, 1.f,
	X1, Y0, Z1,  0.5f, 1.f,

	X1, Y0, Z1,  0.5f, 1.f,
	X0, Y1, Z1,  0.5f, 1.f,

	// Top quad
	X0, Y1, Z1,  0.5f, 1.f,
	X0, Y1, Z0,  0.5f, 1.f,
	X1, Y1, Z1,  0.5f, 1.f,
	X1, Y1, Z0,  0.5f, 1.f,

	X1, Y1, Z0,  0.5f, 1.f,
	X0, Y1, Z1,  0.5f, 1.f,

	// Left quad
	X0, Y1, Z1,  0.5f, 1.f,
	X0, Y0, Z1,  0.5f, 1.f,
	X0, Y1, Z0,  0.5f, 1.f,
	X0, Y0, Z0,  0.5f, 1.f,

	X0, Y0, Z0,  0.5f, 1.f,
	X1, Y1, Z0,  0.5f, 1.f,

	// Right quad
	X1, Y1, Z0,  0.5f, 1.f,
	X1, Y0, Z0,  0.5f, 1.f,
	X1, Y1, Z1,  0.5f, 1.f,
	X1, Y0, Z1,  0.5f, 1.f,
};

/**
 * Helper function to load the raw texture data into an array.
 * @param filename The filename of the texture.
 * @param data A pointer to an array where the data should be stored.
 * @param size A pointer to a variable where to size should be stored.
 * @return 0 on success, non-zero on error.
 */
int readFileContents(char const * const filename, void **data, size_t *size) {
	assert(filename && data && size);

	FILE *f = NULL;

	f = fopen(filename, "rb");

	// Read texture from file
	if(!f) {
		return 1;
	}

	fseek(f, 0, SEEK_END);
	long imageSize = ftell(f);
	rewind(f);

	*data = malloc(imageSize);
	if(!*data) {
		fclose(f);
		return 2;
	}

	fread(*data, 1, imageSize, f);
	fclose(f);

	*size = imageSize;

	return 0;
}

int initGL() {
	glKosInit();

	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);

	glClearColor(0.1, 0.2, 0.4, 1);

	/* Init and bind VAO */
	glGenVertexArrays(1, &vaoID);
	if(!vaoID) {
		return -1;
	}

	glBindVertexArray(vaoID);

	glGenBuffers(1, &bufferID);
	if(!bufferID) {
		return -1;
	}

	glBindBuffer(GL_ARRAY_BUFFER, bufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

	size_t const stride = (3 + 2) * sizeof(GLfloat);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer  (3, GL_FLOAT, stride, BUFFER_OFFSET(0));
	glTexCoordPointer(2, GL_FLOAT, stride, BUFFER_OFFSET(3 * sizeof(float)));

	glBindVertexArray(0);

	// Texture
	size_t imageSize;
	void *data;
	if(readFileContents("/rd/image.vq", &data, &imageSize)) {
		puts("Cannot read texture file.");
		return 1;
	}

	glGenTextures(1, &textureID);
	if(!textureID) {
		puts("Cannot generate texture ID.");
		free(data);
		return 1;
	}

	glBindTexture(GL_TEXTURE_2D, textureID);
	glCompressedTexImage2D(GL_TEXTURE_2D, 0, GL_KOS_RGB565_TWIDDLEDVQ, 1024, 512, 0, imageSize, data);
	free(data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Create the projection matrix
	glMatrixMode(GL_PROJECTION);
	gluPerspective(90, 640.f / 480.f, 1.f, 100.f);
	glMatrixMode(GL_MODELVIEW);

	// Store the transformation matrix to avoid loading an identity matrix,
	// translating and and applying a rotation every frame.
	glTranslatef(0, 0, -2.f);
	glRotatef(-0.7f, 1.f, 0, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, transform);

	return 0;
}

void draw() {
	static float angles[3] = {0};

	angles[0] -= 0.6f;
	angles[1] += 0.02f;
	angles[2] += 0.2f;

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadMatrixf(transform);

	float s, c;
	fsincos(angles[2], &s, &c);
	glTranslatef(c, 0.25f * s, s);

	glRotatef(angles[0], 0, 1, 0);
	glRotatef(angles[1], 0, 0, 1);

	glBindVertexArray(vaoID);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 34);

	glBindVertexArray(0);
}

int main(int argc, char **argv) {
	int error;

	puts("main()");

	if(initGL()) {
		puts("Cannot init!");
		return -1;
	}

	while(1) {
		draw();

		glSwapBuffersKOS();

		error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	glDeleteBuffers(1, &vaoID);

	return 0;
}
