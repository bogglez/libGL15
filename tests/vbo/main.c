#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


#define X0 -0.5
#define X1  0.5
#define Y0 -0.5
#define Y1  0.5
#define Z  -1

/*
 * array buffer object, holding vertex attributes
 * (coordinates, texture coordinates, normals, ...)
 */
unsigned vboID;

/*
 * Vertex attributes will be copied into the vertex buffer object
 */
GLfloat const vertexData[] = {
	/* 3D Coordinate */
	X1, Y1, Z,
	X0, Y1, Z,
	X1, Y0, Z,
	X0, Y0, Z,
};

int initGL() {
	glKosInit();

	glClearColor(0.1, 0.2, 0.4, 1);

	glGenBuffers(1, &vboID);
	if(!vboID) {
		return -1;
	}

	glEnable(GL_CULL_FACE);

	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return 0;
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT);

	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, 0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableClientState(GL_VERTEX_ARRAY);
}

int main(int argc, char **argv) {
	int error;

	puts("main()");

	if(initGL()) {
		puts("Cannot init!");
		return -1;
	}

	while(1) {
		draw();

		glSwapBuffersKOS();

		error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	glDeleteBuffers(1, &vboID);

	return 0;
}
