#include <math.h>
#include <stdio.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif


#define X0 -0.5
#define X1  0.5
#define Y0 -0.5
#define Y1  0.5
#define Z  -1

/* Use this to specify offsets into buffer objects.
 * The address will be interpreted as an integer
 * when buffer objects are bound
 */
#define BUFFER_OFFSET(i) ((char *)NULL + (i))

/* 
 * VAO: Vertex Array Object
 * Saves currently bound array and element array buffer,
 * activated client states (whether normals are used etc),
 * strides, types and count of types (whether texture
 * coordinates have 2 or 4 components etc).
 *
 * With this you can do the following without repeated setup:
 *   glBindVertexArray(vaoID);
 *   glDrawElements(GL_TRIANGLES, 1000, GL_UNSIGNED_SHORT, NULL);
 */
GLuint vaoID;

/*
 * array and element array buffer objects, holding vertex attributes
 * (coordinates, texture coordinates, normals, ...) and indices each.
 */
GLuint bufferIDs[2];

/*
 * Vertex attributes will be copied into the vertex buffer object
 */
GLfloat const vertexData[] = {
	/* Coordinate, Normal, UV */
	X0, Y1, Z,  0, 0, 1,  0, 1, /* top left */
	X0, Y0, Z,  0, 0, 1,  0, 0, /* bottom left */
	X1, Y1, Z,  0, 0, 1,  1, 1, /* top right */
	X1, Y0, Z,  0, 0, 1,  1, 0, /* bottom right */
};

/*
 * Indices will be copied into the index buffer object
 */
GLubyte const indexData[] = {
	0, 1, 2, 3
};

int initGL() {
	glKosInit();

	glEnable(GL_CULL_FACE);

	glClearColor(0.1, 0.2, 0.4, 1);

	/* Init and bind VAO */
	glGenVertexArrays(1, &vaoID);
	if(!vaoID) {
		return -1;
	}

	glBindVertexArray(vaoID);

	GLuint bufferIDs[2];
	glGenBuffers(2, bufferIDs);
	if(!bufferIDs[0] || !bufferIDs[1]) {
		return -1;
	}

	glBindBuffer(GL_ARRAY_BUFFER, bufferIDs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferIDs[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexData), indexData, GL_STATIC_DRAW);

	size_t const stride = (3 + 3 + 2) * sizeof(GLfloat);

	glEnableClientState(GL_VERTEX_ARRAY);
//	glEnableClientState(GL_NORMAL_ARRAY);
//	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, stride, BUFFER_OFFSET(0));
	glNormalPointer(GL_FLOAT, stride, BUFFER_OFFSET(3 * sizeof(float)));
	glTexCoordPointer(2, GL_FLOAT, stride, BUFFER_OFFSET((3+3) * sizeof(float)));

	glBindVertexArray(0);

	return 0;
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT);

	/* Update model view matrix */
	glRotatef(.5f, 0, 0, 1);

	glBindVertexArray(vaoID);
//	glDrawElements(GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, NULL);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glBindVertexArray(0);
}

int main(int argc, char **argv) {
	int error;

	puts("main()");

	if(initGL()) {
		puts("Cannot init!");
		return -1;
	}

	while(1) {
		draw();

		glSwapBuffersKOS();

		error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	glDeleteBuffers(1, &vaoID);

	return 0;
}
