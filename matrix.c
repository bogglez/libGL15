/**
 * @file matrix.c
 * Matrix transformation functions and state.
 */

#include <assert.h>
#include <string.h>
#include <math.h>
#include <dc/fmath.h>
#include <dc/vec3f.h>
#include <dc/matrix.h>
#include <dc/matrix3d.h>
#include "GL/gl.h"
#include "error.h"
#include "immediate.h"
#include "matrix.h"
#include "util.h"


/*
 * State
 */
static GLenum matrixMode = GL_MODELVIEW;

// In NDC the near plane is at -1, far plane at 1.
// This maps the planes to other z values in window space.
static float nearVal = 0;
static float farVal  = 1;

// Note on matrix storage:
// Memory layout is row-major, so
// m[0][0] = first axis x scale
// m[1][0] = first axis y scale
// m[3][0] = first axis translation
// m[0][1] = second axis x scale
// m[3][1] = second axis translation

// Combination for performance when clipping is not needed
// Needs to be updated when modelview, projection or
// ndcToWindow change
static matrix_t modelToWindowMatrix ALIGN32 = {
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}
};

// Combination for performance when clipping is needed
// Needs to be updated when modelview or projection change
static matrix_t modelToClipMatrix ALIGN32 = {
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}
};

// Modified by glViewport
static matrix_t ndcToWindowMatrix ALIGN32 = {
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}
};

// Needs to be updated when modelview changes
/*static matrix_t normalMatrix ALIGN32 = {
	{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}
};
*/

#define MODELVIEW_STACK_DEPTH  32
#define PROJECTION_STACK_DEPTH  2
#define TEXTURE_STACK_DEPTH     2
#define COLOR_STACK_DEPTH       2

static size_t modelViewMatrixCount  = 0;
static size_t projectionMatrixCount = 0;
static size_t textureMatrixCount    = 0;
static size_t colorMatrixCount      = 0;

static matrix_t modelViewMatrixStack[MODELVIEW_STACK_DEPTH] ALIGN32 = {
	{{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}}
};

static matrix_t projectionMatrixStack[PROJECTION_STACK_DEPTH] ALIGN32 = {
	{{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}}
};

static matrix_t textureMatrixStack[TEXTURE_STACK_DEPTH] ALIGN32 = {
	{{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}}
};

static matrix_t colorMatrixStack[COLOR_STACK_DEPTH] ALIGN32 = {
	{{1, 0, 0, 0},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}}
};


static matrix_t *currentMatrix = &modelViewMatrixStack[0];

static int isModelMatrixDirty       = 0;
static int isProjectionMatrixDirty  = 0;
static int isNDCToWindowMatrixDirty = 0;

/*
 * Internal functions
 */
static float cotanf(float x) {
	return tanf((float)M_PI_2 - x);
}

#ifndef NDEBUG
static int isValidMatrixMode(GLenum mode) {
	return mode == GL_MODELVIEW
		|| mode == GL_PROJECTION
		|| mode == GL_TEXTURE
		|| mode == GL_COLOR;
}

static int isValidMatrixName(GLenum mode) {
	return mode == GL_MODELVIEW_MATRIX
		|| mode == GL_PROJECTION_MATRIX
		|| mode == GL_TEXTURE_MATRIX
		|| mode == GL_COLOR_MATRIX;
}
#endif

// When a matrix changes, tag it as dirty
static void onMatrixChanged() {
	switch(matrixMode) {
		case GL_MODELVIEW:  isModelMatrixDirty      = 1; break;
		case GL_PROJECTION: isProjectionMatrixDirty = 1; break;
	}
}


/*
 * Functions
 */
void loadModelToWindowMatrixToRegisters() {
	mat_load(&modelToWindowMatrix);
}

void loadModelToClipMatrixToRegisters() {
	mat_load(&modelToClipMatrix);
}

void loadNDCToWindowMatrixToRegisters() {
	mat_load(&ndcToWindowMatrix);
}

// Calculates result of popular matrix transformation chains when
// one of the matrices in the chain changes
void updateCachedMatrices() {
	if(isModelMatrixDirty || isProjectionMatrixDirty) {
		// Update modelToClipMatrix (used when clipping is required)
		mat_load(&projectionMatrixStack[projectionMatrixCount]);
		mat_apply(&modelViewMatrixStack[modelViewMatrixCount]);
		mat_store(&modelToClipMatrix);
	}

	if(isModelMatrixDirty || isProjectionMatrixDirty || isNDCToWindowMatrixDirty) {
		// Update modelToWindow (used when clipping is not required)
		mat_load(&ndcToWindowMatrix);
		mat_apply(&modelToClipMatrix); // Use updated matrix from above
		mat_store(&modelToWindowMatrix);
	}

	if(isModelMatrixDirty) {
		// Update normal matrix (used for shading)
		/// \todo Need to implement invtranspose(modelViewMatrix)
	}

	isModelMatrixDirty       = 0;
	isProjectionMatrixDirty  = 0;
	isNDCToWindowMatrixDirty = 0;
}

/**
 * Copy one of the OpenGL matrices to a destination buffer.
 * \param matrix The name of the OpenGL matrix: GL_{COLOR, MODELVIEW, PROJECTION, TEXTURE}_MATRIX.
 * \param dest   Where to store the matrix. Must have at least 16 * sizeof(float) memory allocated.
 */
void copyMatrixf(GLenum matrix, float * dest) {
	assert(isValidMatrixName(matrix));
	assert(dest);

	matrix_t *src;

	switch(matrix) {
		case GL_MODELVIEW_MATRIX:  src = &modelViewMatrixStack[modelViewMatrixCount];   break;
		case GL_PROJECTION_MATRIX: src = &projectionMatrixStack[projectionMatrixCount]; break;
		case GL_TEXTURE_MATRIX:    src = &textureMatrixStack[textureMatrixCount];       break;
		case GL_COLOR_MATRIX:      src = &colorMatrixStack[colorMatrixCount];           break;

		default:
		assert(0);
		return;
	}

	memcpy(dest, src, sizeof(float) * 16);
}

/*
 * OpenGL functions
 */
void GLAPIENTRY glMatrixMode(GLenum mode) {
	GL_CHECK(GL_INVALID_ENUM, !isValidMatrixMode(mode));
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	matrixMode = mode;

	switch(mode) {
		case GL_MODELVIEW:  currentMatrix = &modelViewMatrixStack[modelViewMatrixCount]; break;
		case GL_PROJECTION: currentMatrix = &projectionMatrixStack[projectionMatrixCount]; break;
		case GL_TEXTURE:    currentMatrix = &textureMatrixStack[textureMatrixCount]; break;
		case GL_COLOR:      currentMatrix = &colorMatrixStack[colorMatrixCount]; break;
	}
}

void GLAPIENTRY glLoadIdentity(void) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	mat_identity();
	mat_store(currentMatrix);

	onMatrixChanged();
}

// WARNING: reduced precision
void GLAPIENTRY glLoadMatrixd(GLdouble const *m) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	assert(m);

	for(unsigned y = 0; y < 4; ++y) {
		for(unsigned x = 0; x < 4; ++x) {
			(*currentMatrix)[y][x] = (GLfloat) *m++;
		}
	}
	onMatrixChanged();
}

void GLAPIENTRY glLoadMatrixf(GLfloat const *m) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	assert(m);


	for(unsigned y = 0; y < 4; ++y) {
		for(unsigned x = 0; x < 4; ++x) {
			(*currentMatrix)[y][x] = *m++;
		}
	}

	onMatrixChanged();
}

void GLAPIENTRY glLoadTransposeMatrixd(GLdouble const m[16]) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	assert(m);


	for(size_t i = 0; i < 16; ++i) {
		(*currentMatrix)[i>>2][i&3] = (GLfloat) m[i];
	}

	for(unsigned x = 0; x < 4; ++x) {
		for(unsigned y = 0; y < 4; ++y) {
			(*currentMatrix)[x][y] = (GLfloat) *m++;
		}
	}

	onMatrixChanged();
}

void GLAPIENTRY glLoadTransposeMatrixf(GLfloat const m[16]) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	assert(m);


	for(unsigned x = 0; x < 4; ++x) {
		for(unsigned y = 0; y < 4; ++y) {
			(*currentMatrix)[x][y] = *m++;
		}
	}

	onMatrixChanged();
}

void GLAPIENTRY glTranslatef(GLfloat x, GLfloat y, GLfloat z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	mat_load(currentMatrix);
	mat_translate(x, y, z);
	mat_store(currentMatrix);

	onMatrixChanged();
}

void GLAPIENTRY glTranslated(GLdouble x, GLdouble y, GLdouble z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	glTranslatef(x, y, z);
}

void GLAPIENTRY glScalef(GLfloat x, GLfloat y, GLfloat z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	mat_load(currentMatrix);
	mat_scale(x, y, z);
	mat_store(currentMatrix);

	onMatrixChanged();
}

void GLAPIENTRY glScaled(GLdouble x, GLdouble y, GLdouble z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	glScalef(x, y, z);
}



void GLAPIENTRY glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near_val, GLdouble far_val) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, left == right || bottom == top || near_val == far_val);


	float ortho[4][4] ALIGN32 = {{0.f}};
	ortho[0][0] =  2.f                  / (right - left);
	ortho[1][1] =  2.f                  / (top - bottom);
	ortho[2][2] = -2.f                  / (far_val - near_val);
	ortho[3][0] = -(right   + left)     / (right   - left);;
	ortho[3][1] = -(top     + bottom)   / (top     - bottom);
	ortho[3][2] = -(far_val + near_val) / (far_val - near_val);
	ortho[3][3] = 1.f;

	mat_load(currentMatrix);
	mat_apply(&ortho);
	mat_store(currentMatrix);

	onMatrixChanged();
}

void GLAPIENTRY glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near_val, GLdouble far_val) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, left == right || bottom == top || near_val <= 0. || far_val <= 0.);


	float frustum[4][4] ALIGN32 = {{0.f}};
	frustum[0][0] = 2.f * near_val       / (right - left);
	frustum[1][1] = 2.f * near_val       / (top - bottom);
	frustum[2][0] = (right + left) / (right - left);
	frustum[2][1] = (top + bottom) / (top - bottom);
	frustum[2][2] = (near_val - far_val) / (far_val - near_val);
	frustum[2][3] = -1.f;
	frustum[3][2] = 2.f * far_val * near_val / (far_val - near_val);

	mat_load(currentMatrix);
	mat_apply(&frustum);
	mat_store(currentMatrix);

	onMatrixChanged();
}

void GLAPIENTRY glRotatef(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

	float s, c;
	fsincos(angle, &s, &c);

	vec3f_normalize(x, y, z);

	float const cinv = 1.f - c;
	float const xs = s * x;
	float const ys = s * y;
	float const zs = s * z;

	float const xcinv = x * cinv;
	float const ycinv = y * cinv;

	float const xycinv = y * xcinv;
	float const xzcinv = z * xcinv;
	float const yzcinv = z * ycinv;

	float rot[4][4] ALIGN32;
	rot[0][0] = x * xcinv  + c;
	rot[0][1] = xycinv     + zs;
	rot[0][2] = xzcinv     - ys;
	rot[0][3] = 0.f;

	rot[1][0] = xycinv     - zs;
	rot[1][1] = y * ycinv  + c;
	rot[1][2] = yzcinv     + xs;
	rot[1][3] = 0.f;

	rot[2][0] = xzcinv     + ys;
	rot[2][1] = yzcinv     - xs;
	rot[2][2] = z*z * cinv + c;
	rot[2][3] = 0.f;

	rot[3][0] = 0.f;
	rot[3][1] = 0.f;
	rot[3][2] = 0.f;
	rot[3][3] = 1.f;

	mat_load(currentMatrix);
	mat_apply(&rot);
	mat_store(currentMatrix);

	onMatrixChanged();
}

void GLAPIENTRY glRotated(GLdouble angle, GLdouble x, GLdouble y, GLdouble z) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


	glRotatef(angle, x, y, z);
}

void GLAPIENTRY glViewport(GLint x, GLint y, GLsizei width, GLsizei height) {
	GL_CHECK(GL_INVALID_VALUE, width < 0 || height < 0);
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());


#ifdef LIBGL15_FSAA
	float const w2 = width;
#else
	float const w2 = width/2.f;
#endif
	float const h2 = height/2.f;

	ndcToWindowMatrix[0][0] = w2;
	ndcToWindowMatrix[1][1] = -h2;
	ndcToWindowMatrix[3][0] = x + w2;
	ndcToWindowMatrix[3][1] = y + h2;

	isNDCToWindowMatrixDirty = 1;
}

void GLAPIENTRY glDepthRange(GLclampd near_val, GLclampd far_val) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	// No need to check for near < far, reverse mappings are allowed


	// nearVal must be > 0 on PVR
	float const eps = 0.00000003f;
	nearVal = CLAMP(near_val, eps, 1);
	farVal  = CLAMP1(far_val);

	ndcToWindowMatrix[2][2] = (farVal - nearVal)/2.f;
	ndcToWindowMatrix[3][2] = (farVal + nearVal)/2.f;

	isNDCToWindowMatrixDirty = 1;
}

void glPushMatrix() {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_STACK_OVERFLOW,
		(matrixMode == GL_MODELVIEW  && modelViewMatrixCount >= MODELVIEW_STACK_DEPTH)   ||
		(matrixMode == GL_PROJECTION && projectionMatrixCount >= PROJECTION_STACK_DEPTH) ||
		(matrixMode == GL_TEXTURE    && textureMatrixCount >= TEXTURE_STACK_DEPTH)       ||
		(matrixMode == GL_COLOR      && colorMatrixCount >= COLOR_STACK_DEPTH)
	);

	memcpy(currentMatrix + 1, currentMatrix, sizeof(matrix_t));
	++currentMatrix;

	switch(matrixMode) {
		case GL_MODELVIEW:  ++modelViewMatrixCount;  break;
		case GL_PROJECTION: ++projectionMatrixCount; break;
		case GL_TEXTURE:    ++textureMatrixCount;    break;
		case GL_COLOR:      ++colorMatrixCount;      break;
	}

	// No need to call onMatrixChanged() because the new matrix is the same.
}

void glPopMatrix() {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_STACK_UNDERFLOW,
		(matrixMode == GL_MODELVIEW  && !modelViewMatrixCount)  ||
		(matrixMode == GL_PROJECTION && !projectionMatrixCount) ||
		(matrixMode == GL_TEXTURE    && !textureMatrixCount)    ||
		(matrixMode == GL_COLOR      && !colorMatrixCount)
	);

	--currentMatrix;

	switch(matrixMode) {
		case GL_MODELVIEW:  --modelViewMatrixCount;  break;
		case GL_PROJECTION: --projectionMatrixCount; break;
		case GL_TEXTURE:    --textureMatrixCount;    break;
		case GL_COLOR:      --colorMatrixCount;      break;
	}

	onMatrixChanged();
}
