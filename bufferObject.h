/**
 * @file bufferObject.h
 * @brief OpenGL buffer object related functions.
 */

#pragma once

#include "GL/gl.h"


/**
 * Describes how an OpenGL buffer object is/was bound with glBindBuffer().
 */
typedef enum {
	BUFFEROBJECTSTATE_UNUSED,

	// State after glGenBuffers, before first glBindBuffer
	BUFFEROBJECTSTATE_NEVERBOUND,

	// After first glBindBuffer
	BUFFEROBJECTSTATE_ACTIVE,
} BufferObjectState;


/**
 * State of a generic OpenGL buffer object.
 * This may be a
 * - vertex buffer object (VBO),
 * - element array buffer object (IBO),
 * - framebuffer object (FBO).
 */
typedef struct {
	BufferObjectState state;  /// Whether the BO is bound and how.
	GLenum            target; /// What kind of BO this is.
	GLenum            usage;  /// Usage hints for performance.
	GLenum            access; /// Read-write access information for performance.
	void*             data;   /// The contained raw data.
	GLsizeiptr        size;   /// How large the data is.
} BufferObject;


/*
 * Functions
 */

/**
 * Initializes all buffer object state.
 * Used during OpenGL buffer creation.
 * @param requestedBufferObjectCount How many buffer objects should be allocated.
 * @return 0 on success, non-zero on failure.
 */
int initBufferObjects(size_t requestedBufferObjectCount);

/**
 * Returns the state of the buffer object currently bound to the target.
 * @param target The target of which the buffer object should be returned.
 * @return The state of the buffer object currently bound to the target.
 */
BufferObject * getActiveBufferObjectForTarget(GLenum target);

/**
 * Returns the buffer object state of the buffer object with the provided ID.
 * @param buffer The ID of the desired buffer object.
 * @return The state of the buffer object with the provided ID or NULL if the ID is invalid.
 */
BufferObject * getBufferObjectByID(GLuint buffer);

/**
 * Returns the ID of the buffer object currently bound to the target.
 * @param target The buffer object target for which the buffer object ID should be returned.
 * @return The ID of the buffer object bound to the provided buffer object target.
 */
GLuint getActiveBufferObjectIDForTarget(GLenum target);
