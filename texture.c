/**
 * @file texture.c
 * Texture-related functions and state.
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <dc/pvr.h>
#include "GL/gl.h"
#include "draw.h"
#include "error.h"
#include "immediate.h"
#include "texture.h"


#define TEXTURE_UNIT_MAX_COUNT 3

/// Multiple of 8 due to `usedTextureIDs'
#define TEXTURE_MAX_COUNT      256

#define TEXTURE_DIMENSION_MAX  1024

/// log2(TEXTURE_DIMENSION_MAX) mipmap levels
#define TEXTURE_LEVEL_COUNT    10


uint8_t usedTextureIDs[TEXTURE_MAX_COUNT / 8] = {0};

static Texture textures[TEXTURE_MAX_COUNT];

static unsigned activeTextureUnit = 0;
static TextureUnit textureUnits[TEXTURE_UNIT_MAX_COUNT];


/* Internal functions */
#ifndef NDEBUG
static int isTextureTarget(GLenum target) {
	return target == GL_TEXTURE_1D
		|| target == GL_TEXTURE_2D
		|| target == GL_TEXTURE_3D
		|| target == GL_TEXTURE_CUBE_MAP;
}

static int isTexture2DTarget(GLenum mode) {
	return mode == GL_TEXTURE_2D
		|| mode == GL_PROXY_TEXTURE_2D
		|| mode == GL_PROXY_TEXTURE_CUBE_MAP
		|| mode == GL_TEXTURE_CUBE_MAP_POSITIVE_X
		|| mode == GL_TEXTURE_CUBE_MAP_NEGATIVE_X
		|| mode == GL_TEXTURE_CUBE_MAP_POSITIVE_Y
		|| mode == GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
		|| mode == GL_TEXTURE_CUBE_MAP_POSITIVE_Z
		|| mode == GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
}

static int isTexelType(GLenum type) {
	return type == GL_UNSIGNED_BYTE
		|| type == GL_BYTE
		|| type == GL_BITMAP
		|| type == GL_UNSIGNED_SHORT
		|| type == GL_SHORT
		|| type == GL_UNSIGNED_INT
		|| type == GL_INT
		|| type == GL_FLOAT
		|| type == GL_UNSIGNED_BYTE_3_3_2
		|| type == GL_UNSIGNED_BYTE_2_3_3_REV
		|| type == GL_UNSIGNED_SHORT_5_6_5
		|| type == GL_UNSIGNED_SHORT_5_6_5_REV
		|| type == GL_UNSIGNED_SHORT_4_4_4_4
		|| type == GL_UNSIGNED_SHORT_4_4_4_4_REV
		|| type == GL_UNSIGNED_SHORT_5_5_5_1
		|| type == GL_UNSIGNED_SHORT_1_5_5_5_REV
		|| type == GL_UNSIGNED_INT_8_8_8_8
		|| type == GL_UNSIGNED_INT_8_8_8_8_REV
		|| type == GL_UNSIGNED_INT_10_10_10_2
		|| type == GL_UNSIGNED_INT_2_10_10_10_REV;
}

static int isInternalTextureFormat(GLint internalFormat) {
	return (internalFormat >= 1 && internalFormat <= 4)
		|| internalFormat == GL_ALPHA
		|| internalFormat == GL_ALPHA4
		|| internalFormat == GL_ALPHA8
		|| internalFormat == GL_ALPHA12
		|| internalFormat == GL_ALPHA16
		|| internalFormat == GL_COMPRESSED_ALPHA
		|| internalFormat == GL_COMPRESSED_LUMINANCE
		|| internalFormat == GL_COMPRESSED_LUMINANCE_ALPHA
		|| internalFormat == GL_COMPRESSED_INTENSITY
		|| internalFormat == GL_COMPRESSED_RGB
		|| internalFormat == GL_COMPRESSED_RGBA
		|| internalFormat == GL_DEPTH_COMPONENT
		|| internalFormat == GL_DEPTH_COMPONENT16
		|| internalFormat == GL_DEPTH_COMPONENT24
		|| internalFormat == GL_DEPTH_COMPONENT32
		|| internalFormat == GL_LUMINANCE
		|| internalFormat == GL_LUMINANCE4
		|| internalFormat == GL_LUMINANCE8
		|| internalFormat == GL_LUMINANCE12
		|| internalFormat == GL_LUMINANCE16
		|| internalFormat == GL_LUMINANCE_ALPHA
		|| internalFormat == GL_LUMINANCE4_ALPHA4
		|| internalFormat == GL_LUMINANCE6_ALPHA2
		|| internalFormat == GL_LUMINANCE8_ALPHA8
		|| internalFormat == GL_LUMINANCE12_ALPHA4
		|| internalFormat == GL_LUMINANCE12_ALPHA12
		|| internalFormat == GL_LUMINANCE16_ALPHA16
		|| internalFormat == GL_INTENSITY
		|| internalFormat == GL_INTENSITY4
		|| internalFormat == GL_INTENSITY8
		|| internalFormat == GL_INTENSITY12
		|| internalFormat == GL_INTENSITY16
		|| internalFormat == GL_R3_G3_B2
		|| internalFormat == GL_RGB
		|| internalFormat == GL_RGB4
		|| internalFormat == GL_RGB5
		|| internalFormat == GL_RGB8
		|| internalFormat == GL_RGB10
		|| internalFormat == GL_RGB12
		|| internalFormat == GL_RGB16
		|| internalFormat == GL_RGBA
		|| internalFormat == GL_RGBA2
		|| internalFormat == GL_RGBA4
		|| internalFormat == GL_RGB5_A1
		|| internalFormat == GL_RGBA8
		|| internalFormat == GL_RGB10_A2
		|| internalFormat == GL_RGBA12
		|| internalFormat == GL_RGBA16
		|| internalFormat == GL_KOS_ARGB1555_TWIDDLEDVQ
		|| internalFormat == GL_KOS_ARGB4444_TWIDDLEDVQ
		|| internalFormat == GL_KOS_RGB565_TWIDDLEDVQ
		|| internalFormat == GL_KOS_YUV422_TWIDDLEDVQ
		|| internalFormat == GL_KOS_RGB565;
}

static int isValidTex2DDimension(GLsizei width, GLsizei height) {
	return width >= 0 && height >= 0 && width <= 2 + TEXTURE_DIMENSION_MAX && height <= 2 + TEXTURE_DIMENSION_MAX;
}

static int isValidMipMapLevel(GLint level) {
	return level >= 0 && level <= log2(TEXTURE_DIMENSION_MAX);
}

static int isTexEnvTarget(GLenum target) {
	return target == GL_TEXTURE_ENV_MODE
		|| target == GL_TEXTURE_FILTER_CONTROL;
}

static int isTexEnvParamName(GLenum pname) {
	return pname == GL_TEXTURE_ENV_MODE
		|| pname == GL_TEXTURE_LOD_BIAS
		|| pname == GL_COMBINE_RGB
		|| pname == GL_COMBINE_ALPHA
		|| pname == GL_SRC0_RGB
		|| pname == GL_SRC1_RGB
		|| pname == GL_SRC2_RGB
		|| pname == GL_SRC0_ALPHA
		|| pname == GL_SRC1_ALPHA
		|| pname == GL_SRC2_ALPHA
		|| pname == GL_OPERAND0_RGB
		|| pname == GL_OPERAND1_RGB
		|| pname == GL_OPERAND2_RGB
		|| pname == GL_OPERAND0_ALPHA
		|| pname == GL_OPERAND1_ALPHA
		|| pname == GL_OPERAND2_ALPHA
		|| pname == GL_RGB_SCALE
		|| pname == GL_ALPHA_SCALE;
}

/**
 * Checks the parameters for glTexImage2D().
 * \return 0 if valid, non-zero if invalid.
 */
static int checkUncompressedTexImage2DParams(GLenum const target, GLint const level, GLint const internalFormat, GLsizei const width, GLsizei const height, GLint const border, GLenum const format, GLenum const type, GLvoid const * const data) {
	if(!isTexture2DTarget(target)) {
		glSetError(GL_INVALID_ENUM);
		return 1;
	}

	/// \todo - GL_INVALID_ENUM is generated if target is one of the six cube map 2D image targets and the width and height parameters are not equal.

	if(!isValidTex2DDimension(width, height)) {
		glSetError(GL_INVALID_VALUE);
		return 1;
	}

	if(!isValidMipMapLevel(level)) {
		glSetError(GL_INVALID_VALUE);
		return 1;
	}

	if(!isInternalTextureFormat(internalFormat)) {
		glSetError(GL_INVALID_VALUE);
		return 7;
	}

	/// \todo - GL_INVALID_VALUE is generated if non-power-of-two textures are not supported  and the width or height cannot be represented as 2^k + 2 (border) for some integer value of k.

	if(border < 0 || border > 1) {
		glSetError(GL_INVALID_VALUE);
		return 8;
	}

	if(format != GL_RGB
		&& (type == GL_UNSIGNED_BYTE_3_3_2
			|| type == GL_UNSIGNED_BYTE_2_3_3_REV
			|| type == GL_UNSIGNED_SHORT_5_6_5
			|| type == GL_UNSIGNED_SHORT_5_6_5_REV
		)
	) {
		glSetError(GL_INVALID_OPERATION);
		return 9;
	}

	if((format != GL_RGBA && format != GL_BGRA)
		&& (type == GL_UNSIGNED_SHORT_4_4_4_4
			|| type == GL_UNSIGNED_SHORT_4_4_4_4_REV
			|| type == GL_UNSIGNED_SHORT_5_5_5_1
			|| type == GL_UNSIGNED_SHORT_1_5_5_5_REV
			|| type == GL_UNSIGNED_INT_8_8_8_8
			|| type == GL_UNSIGNED_INT_8_8_8_8_REV
			|| type == GL_UNSIGNED_INT_10_10_10_2
			|| type == GL_UNSIGNED_INT_2_10_10_10_REV
		)
	) {
		glSetError(GL_INVALID_OPERATION);
		return 10;
	}

	if((target != GL_TEXTURE_2D && target != GL_PROXY_TEXTURE_2D)
		&& (internalFormat == GL_DEPTH_COMPONENT
			|| internalFormat == GL_DEPTH_COMPONENT16
			|| internalFormat == GL_DEPTH_COMPONENT24
			|| internalFormat == GL_DEPTH_COMPONENT32
		)
	) {
		glSetError(GL_INVALID_OPERATION);
		return 11;
	}

	if(format == GL_DEPTH_COMPONENT
		&& (internalFormat != GL_DEPTH_COMPONENT
			&& internalFormat != GL_DEPTH_COMPONENT16
			&& internalFormat != GL_DEPTH_COMPONENT24
			&& internalFormat != GL_DEPTH_COMPONENT32
		)
	) {
		glSetError(GL_INVALID_OPERATION);
		return 12;
	}

	if(format != GL_DEPTH_COMPONENT
		&& (internalFormat == GL_DEPTH_COMPONENT
			|| internalFormat == GL_DEPTH_COMPONENT16
			|| internalFormat == GL_DEPTH_COMPONENT24
			|| internalFormat == GL_DEPTH_COMPONENT32
		)
	) {
		glSetError(GL_INVALID_OPERATION);
		return 13;
	}

	if(!isTexelType(type)) {
		glSetError(GL_INVALID_ENUM);
		return 14;
	}

	/// \todo
	/// - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to the GL_PIXEL_UNPACK_BUFFER target and the buffer object's data store is currently mapped.
	/// - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to the GL_PIXEL_UNPACK_BUFFER target and the data would be unpacked from the buffer object such that the memory reads required would exceed the data store size.
	/// - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to the GL_PIXEL_UNPACK_BUFFER target and data is not evenly divisible into the number of bytes needed to store in memory a datum indicated by type.

	if(isImmediateModeActive()) {
		glSetError(GL_INVALID_OPERATION);
		return 17;
	}

	return 0;
}

/**
 * Checks the parameters for glCompressedTexImage2D().
 * \return 0 if valid, non-zero if invalid.
 */
static int checkCompressedTexImage2DParams(GLenum const target, GLint const level, GLenum const internalFormat, GLsizei const width, GLsizei const height, GLint const border, GLsizei const imageSize, GLvoid const * const data) {
	if(!isTexture2DTarget(target)) {
		glSetError(GL_INVALID_ENUM);
		return 1;
	}

	if(border < 0 || border > 1) {
		glSetError(GL_INVALID_VALUE);
		return 1;
	}

	// Generic compressed formats are forbidden
	if(internalFormat == GL_COMPRESSED_ALPHA
			|| internalFormat == GL_COMPRESSED_LUMINANCE
			|| internalFormat == GL_COMPRESSED_LUMINANCE_ALPHA
			|| internalFormat == GL_COMPRESSED_INTENSITY
			|| internalFormat == GL_COMPRESSED_RGB
			|| internalFormat == GL_COMPRESSED_RGBA) {
		return 1;
	}

	if(!isValidTex2DDimension(width, height)) {
		glSetError(GL_INVALID_VALUE);
		return 1;
	}

	if(!isValidMipMapLevel(level)) {
		glSetError(GL_INVALID_VALUE);
		return 1;
	}

	/// \todo
	/// - GL_INVALID_OPERATION is generated if parameter combinations are not supported by the specific compressed internal format as specified in the specific texture compression extension.
	/// - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to the GL_PIXEL_UNPACK_BUFFER target and the buffer object's data store is currently mapped.
	/// - GL_INVALID_OPERATION is generated if a non-zero buffer object name is bound to the GL_PIXEL_UNPACK_BUFFER target and the data would be unpacked from the buffer object such that the memory reads required would exceed the data store size.

	if(isImmediateModeActive()) {
		glSetError(GL_INVALID_OPERATION);
		return 1;
	}


	return 0;
}


#endif

/**
 * Determines how a texture format is stored internally. For example
 * an GL_RGBA texture may be stored as GL_RGBA4 internally for performance.
 * @param textureFormatHint The source texture format to convert.
 * @return The internal representation of the texture format.
 */
static GLenum chooseInternalFormat(GLenum textureFormatHint) {
	switch(textureFormatHint) {
		case GL_RGBA:
		case GL_RGBA2:
		case GL_RGBA4:
		case GL_RGBA12:
		case GL_RGBA16:
		return GL_RGBA4;

		case GL_RGB:
		case GL_RGB4:
		case GL_RGB5:
		case GL_RGB8:
		case GL_RGB10:
		case GL_RGB12:
		case GL_RGB16:
		return GL_KOS_RGB565;

		case GL_RGB10_A2:
		case GL_RGB5_A1:
		return GL_RGB5_A1;
	}


#ifndef NDEBUG
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif

	assert(0);
	return GL_NONE;
}

/**
 * Calculates the image size of a texture in a certain texture format in bytes.
 * @param w The texture's width.
 * @param h The texture's height.
 * @param textureFormat The texture's format.
 * @return The size of the texture in bytes.
 */
static size_t calculateImageSize(size_t w, size_t h, GLenum textureFormat) {
	assert(w && h);
	assert(isInternalTextureFormat(textureFormat));

	switch(textureFormat) {
		case GL_KOS_RGB565:
		case GL_RGBA4:
		case GL_RGB5_A1:
		return 2 * w * h;
	}

#ifndef NDEBUG
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif
	assert(0);
	return 0;
}

/**
 * Copies a texture of a given format to an internal representation in VRAM, converting if necessary.
 * @param dest       The location in VRAM at which the internal texture should be stored.
 * @param destSize   Size of the internal texture in bytes.
 * @param src        The location of the source texture in RAM.
 * @param w          The width of the source texture.
 * @param h          The height of the source texture.
 * @param srcFormat  The texture format of the source texture, e.g. GL_RGB.
 * @param srcType    The data type of the source texture's texels, e.g. GL_UNSIGNED_BYTE.
 * @param destFormat The texture format  of the internal texture, e.g. GL_RGB5_A1.
 * @return 0 on succes, non-zero when Copying was not successful.
 */
static int copyTexture(pvr_ptr_t dest, size_t destSize, void const *src, size_t w, size_t h, GLenum const srcFormat, GLenum const srcType, GLenum const destFormat) {
	assert(dest);
	assert(src);
	assert(w && h);
	assert(srcFormat  != GL_NONE);
	assert(srcType    != GL_NONE);
	assert(destFormat != GL_NONE);

	// When the source-to-internal mapping is an exact match do a direct copy.
	/// \fixme Add more texture types which can be copied directly
	if(
		(srcFormat == GL_RGB  && srcType == GL_UNSIGNED_SHORT_5_6_5   && destFormat == GL_KOS_RGB565) ||
		(srcFormat == GL_RGBA && srcType == GL_UNSIGNED_SHORT_1_5_5_5 && destFormat == GL_RGB5_A1) ||
		(srcFormat == GL_RGBA && srcType == GL_UNSIGNED_SHORT_4_4_4_4 && destFormat == GL_RGBA4)
	) {
		memcpy(dest, src, destSize);
	}

	// Convert RGB8 to RGB565
	else if(srcFormat == GL_RGB && srcType == GL_UNSIGNED_BYTE && destFormat == GL_KOS_RGB565) {
		uint16_t *internalTexel = dest;
		uint16_t const *end = dest + destSize;
		uint8_t const *texel = src;

		while(internalTexel != end) {
				uint8_t const r = texel[0] >> 3;
				uint8_t const g = texel[1] >> 2;
				uint8_t const b = texel[2] >> 3;
				*internalTexel = (r << 11) | (g << 5) | b;
				++internalTexel;
				texel += 3;
		}
	}
	else if(srcFormat == GL_RGBA && srcType == GL_UNSIGNED_BYTE && destFormat == GL_RGBA4) {
		uint16_t *internalTexel = dest;
		uint16_t const *end = dest + destSize;
		uint8_t const *texel = src;

		while(internalTexel != end) {
				uint8_t const r = texel[0] >> 4;
				uint8_t const g = texel[1] >> 4;
				uint8_t const b = texel[2] >> 4;
				uint8_t const a = texel[3] >> 4;
				*internalTexel = (a << 12) | (r << 8) | (g << 4) | b;
				++internalTexel;
				texel += 4;
		}
	}

	// Conversion necessary but not implemented, or invalid types
	else {
		return 1;
	}

	return 0;
}

/**
 * Get a texture state object by a textureID.
 * @param textureID A number in the range (0, TEXTURE_MAX_COUNT].
 */ 
static Texture* getTextureByID(GLuint const textureID) {
	assert(textureID > 0 && textureID <= TEXTURE_MAX_COUNT);
	return &textures[textureID - 1];
}

/**
 * Marks a texture ID as used/unused.
 * Texture IDs are considered to be used when they have been generated before by glGenTextures() and they have not been freed by glDeleteTextures() yet.
 * @param textureID The texture ID to mark as used/unused.
 */
static void markTextureIDAsUsed(GLuint textureID, int used) {
	assert(textureID > 0 && textureID <= TEXTURE_MAX_COUNT);

	--textureID;
	if(used) {
		usedTextureIDs[textureID / 8] |= 1 << (textureID % 8);
	}
	else {
		usedTextureIDs[textureID / 8] &= ~(1 << (textureID % 8));
	}
}

/**
 * Checks whether a texture ID has been generated before and has not been deleted yet.
 * @param textureID The texture ID to check.
 */
static int isTextureIDUsed(GLuint textureID) {
	assert(textureID > 0 && textureID <= TEXTURE_MAX_COUNT);

	--textureID;
	return usedTextureIDs[textureID / 8] & (1 << (textureID % 8));
}

/**
 * Generates a free texture ID and marks it as used.
 * @return Positive texture ID, 0 on error.
 */
static GLuint generateFreeTextureID() {
	GLuint textureID;
	for(textureID = 1; textureID <= TEXTURE_MAX_COUNT && isTextureIDUsed(textureID); ++textureID);

	if(textureID <= TEXTURE_MAX_COUNT) {
		markTextureIDAsUsed(textureID, 1);
		return textureID;
	}

	return 0;
}

/**
 * Initializes a texture with its default values.
 * @param texture The texture to initialize.
 */
static void initTexture(Texture *texture) {
	assert(texture);

	memset(texture, 0, sizeof(Texture));
	texture->internalFormat   = GL_NONE;
	texture->format           = GL_NONE;
	texture->minFilter        = GL_NEAREST_MIPMAP_LINEAR;
	texture->magFilter        = GL_LINEAR;
	texture->minLOD           = -1000;
	texture->maxLOD           =  1000;
	texture->maxLevel         =  1000;
	texture->wrapS            = GL_REPEAT;
	texture->wrapT            = GL_REPEAT;
	texture->wrapR            = GL_REPEAT;
	texture->compareMode      = GL_NONE; /// \todo - Research default compare mode value.
	texture->compareFunc      = GL_GREATER; /// \todo - Research default compare func value.
	texture->depthTextureMode = GL_LUMINANCE;
	texture->generateMipMap   = GL_FALSE;
}

/**
 * Returns the currently bound texture for the currently bound texture unit.
 * @return The currently active texture in the currently active texture unit or
 *         NULL if no texture is bound to the current texture unit.
 */
static Texture* getActiveTexture() {
	GLuint const boundTextureID = getActiveTextureUnit()->boundTextureID;
	assert(boundTextureID < TEXTURE_MAX_COUNT);
	return boundTextureID > 0 ? getTextureByID(boundTextureID) : NULL;
}


/*
 * Functions
 */
unsigned getActiveTextureUnitNumber() {
	assert(activeTextureUnit < TEXTURE_UNIT_MAX_COUNT);
	return activeTextureUnit;
}

TextureUnit * getActiveTextureUnit() {
	return &textureUnits[activeTextureUnit];
}

void initTextures() {
	memset(usedTextureIDs, 0, sizeof(usedTextureIDs));

	for(GLuint textureID = 1; textureID <= TEXTURE_MAX_COUNT; ++textureID) {
		initTexture(getTextureByID(textureID));
	}
}

void initTextureUnits() {
	memset(textureUnits, 0, sizeof(textureUnits));

	for(size_t i = 0; i < TEXTURE_UNIT_MAX_COUNT; ++i) {
		TextureUnit *unit = &textureUnits[i];
		unit->envMode = GL_MODULATE;
	}
}

unsigned convertGLTextureFormatToPVR(GLenum textureFormat) {
	switch(textureFormat) {
		// Twiddle + VQ (twiddled by default)
		case GL_KOS_ARGB1555_TWIDDLEDVQ:
		return PVR_TXRFMT_VQ_ENABLE | PVR_TXRFMT_TWIDDLED | PVR_TXRFMT_ARGB1555;

		case GL_KOS_ARGB4444_TWIDDLEDVQ:
		return PVR_TXRFMT_VQ_ENABLE | PVR_TXRFMT_TWIDDLED | PVR_TXRFMT_ARGB4444;

		case GL_KOS_RGB565_TWIDDLEDVQ:
		return PVR_TXRFMT_VQ_ENABLE | PVR_TXRFMT_TWIDDLED | PVR_TXRFMT_RGB565;

		case GL_KOS_YUV422_TWIDDLEDVQ:
		return PVR_TXRFMT_VQ_ENABLE | PVR_TXRFMT_TWIDDLED | PVR_TXRFMT_YUV422;


		// No-Twiddle + No-VQ (twiddled is default on PVR)
		/// \fixme Instead of using non-twiddled textures, twiddle on load?
		case GL_NONE:
		return PVR_TXRFMT_NONE;

		case GL_KOS_RGB565:
		return PVR_TXRFMT_RGB565 | PVR_TXRFMT_NONTWIDDLED;

		case GL_RGB5_A1:
		return PVR_TXRFMT_ARGB1555 | PVR_TXRFMT_NONTWIDDLED;

		case GL_RGBA4:
		return PVR_TXRFMT_ARGB4444 | PVR_TXRFMT_NONTWIDDLED;

		/// \todo Add other supported texture formats.
	}

#ifndef NDEBUG
	glSetError(GL_KOS_UNIMPLEMENTED);
#endif

	assert(0);
	return PVR_TXRFMT_NONE;
}


/*
 * OpenGL functions
 */
void glActiveTexture(GLenum texture) {
	GL_CHECK(GL_INVALID_ENUM, texture < GL_TEXTURE0 || texture >= GL_TEXTURE0 + TEXTURE_UNIT_MAX_COUNT);


	activeTextureUnit = texture - GL_TEXTURE0;
}

void glBindTexture(GLenum target, GLuint textureID) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isTextureTarget(target));

#ifndef NDEBUG
	// Allow any target for unspecified textures and allow targets matching texture's
	if(textureID) {
		GLenum const textureTarget = getTextureByID(textureID)->target;
		if(textureTarget && target != textureTarget) {
			glSetError(GL_INVALID_OPERATION);
			return;
		}
	}
#endif

	TextureUnit * const textureUnit = getActiveTextureUnit();

	if(target == GL_TEXTURE_2D) {
		textureUnit->boundTextureID = textureID;
	}
	else {
#ifndef NDEBUG
		/// \todo Implement other texture targets.
		glSetError(GL_KOS_UNIMPLEMENTED);
#endif
		return;
	}

	useTexture(textureID ? getTextureByID(textureID) : NULL);
}

void glGenTextures(GLsizei n, GLuint *textureIDs) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	GLuint id;

	while(n--) {
		id = generateFreeTextureID();

		/* This is not in the spec, but it seems cruel not to report this */
		if(!id) {
#ifndef NDEBUG
			glSetError(GL_OUT_OF_MEMORY);
#endif
			return;
		}

		*textureIDs++ = id;
	}
}

void glDeleteTextures(GLsizei n, const GLuint *textureIDs) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	GLuint const activeTextureID = getActiveTextureUnit()->boundTextureID;

	for(; n--; ++textureIDs) {
		GLuint const textureID = *textureIDs;

		// Ignore invalid texture IDs according to spec
		if(!glIsTexture(textureID)) {
			continue;
		}

		Texture *texture = getTextureByID(textureID);
		pvr_mem_free(texture->data);
		markTextureIDAsUsed(textureID, 0);
		initTexture(texture);

		if(textureID == activeTextureID) {
			useTexture(NULL);
		}
	}
}


void glTexImage2D(GLenum target, GLint level, GLint internalFormatHint, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, GLvoid const *data) {
	int error = 0;
	pvr_ptr_t pvrData = NULL;
	GLenum const internalFormat = chooseInternalFormat(internalFormatHint);
	size_t const imageSize = calculateImageSize(width, height, internalFormat);

#ifndef NDEBUG
	if(checkUncompressedTexImage2DParams(target, level, internalFormatHint, width, height, border, format, type, data)) {
		error = 1;
		goto cleanup;
	}

	if(target != GL_TEXTURE_2D) {
		/// \todo Implement other texture targets.
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}

	/// \todo Implement mipmapping
	if(level) {
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}

	// Choose actual internal format based upon hint
	if(internalFormat == GL_NONE) {
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}

	if(!imageSize) {
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}
#endif

	pvrData = pvr_mem_malloc(imageSize);
	if(!pvrData) {
#ifndef NDEBUG
		glSetError(GL_OUT_OF_MEMORY);
#endif
		error = 1;
		goto cleanup;
	}

	if(copyTexture(pvrData, imageSize, data, width, height, format, type, internalFormat)) {
#ifndef NDEBUG
		glSetError(GL_KOS_UNIMPLEMENTED);
#endif
		assert(0);
		error = 1;
		goto cleanup;
	}

	Texture * const texture = getActiveTexture();
	texture->target = target;
	texture->border = border;
	texture->width  = width;
	texture->height = height;
	texture->internalFormat = internalFormat;
	texture->data = pvrData;

	useTexture(texture);

cleanup:
	if(error) {
		pvr_mem_free(pvrData);
	}
}

void glCompressedTexImage2D(GLenum target, GLint level, GLenum internalFormat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, GLvoid const *data) {
	int error = 0;
	pvr_ptr_t pvrData = NULL;

	Texture *texture = getActiveTexture();

#ifndef NDEBUG
	if(!texture) {
		/// \todo - Find out the proper error value, couldn't find this error case in the docs.
		glSetError(GL_INVALID_VALUE);
		error = 1;
		goto cleanup;
	}

	if(checkCompressedTexImage2DParams(target, level, internalFormat, width, height, border, imageSize, data)) {
		error = 1;
		goto cleanup;
	}

	// \todo - Implement border.
	if(border) {
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}


	if(
		   internalFormat != GL_KOS_ARGB1555_TWIDDLEDVQ
		&& internalFormat != GL_KOS_ARGB4444_TWIDDLEDVQ
		&& internalFormat != GL_KOS_RGB565_TWIDDLEDVQ
		&& internalFormat != GL_KOS_YUV422_TWIDDLEDVQ
	) {
		glSetError(GL_KOS_UNIMPLEMENTED);
		error = 1;
		goto cleanup;
	}
#endif

	// Copy data to VRAM
	pvrData = pvr_mem_malloc(imageSize);
	if(!pvrData) {
#ifndef NDEBUG
		glSetError(GL_OUT_OF_MEMORY);
		assert(0);
#endif
		error = 1;
		goto cleanup;
	}

	memcpy(pvrData, data, imageSize);

	texture->target = target;
	texture->border = border;
	texture->width  = width;
	texture->height = height;
	texture->internalFormat = internalFormat;
	texture->data = pvrData;

	useTexture(texture);

	/// \todo - Mipmapping

cleanup:
	if(error) {
		pvr_mem_free(pvrData);
	}
}

void glTexParameteri(GLenum target, GLenum pname, GLint param) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());

#ifndef NDEBUG
	if(target != GL_TEXTURE_2D) {
		/// \todo Implement more texture targets.
		glSetError(GL_KOS_UNIMPLEMENTED);
		return;
	}
#endif


	Texture * const texture = getActiveTexture();

	if(target == GL_TEXTURE_2D) {
		switch(pname) {
			case GL_TEXTURE_MAG_FILTER:
			case GL_TEXTURE_MIN_FILTER:
				switch(param) {
					case GL_LINEAR: texture->filter = PVR_FILTER_BILINEAR; break;
					case GL_NEAREST: texture->filter = PVR_FILTER_NEAREST; break;

					/* Compatabile with deprecated kgl
					case GL_FILTER_NONE: texture->filter = PVR_FILTER_NONE; break;
					case GL_FILTER_BILINEAR: texture->filter = PVR_FILTER_BILINEAR; break;
					*/

					default:
#ifndef NDEBUG
						glSetError(GL_INVALID_ENUM);
#endif
						return;
				}
				break;

			case GL_TEXTURE_WRAP_S:
				switch(param) {
					case GL_CLAMP:
					case GL_CLAMP_TO_BORDER:
					case GL_CLAMP_TO_EDGE:
					case GL_MIRRORED_REPEAT:
					case GL_REPEAT:
						texture->wrapS = param;
						break;

					default:
#ifndef NDEBUG
						glSetError(GL_INVALID_ENUM);
#endif
						return;
				}
				break;

			case GL_TEXTURE_WRAP_T:
				switch(param) {
					case GL_CLAMP:
					case GL_CLAMP_TO_BORDER:
					case GL_CLAMP_TO_EDGE:
					case GL_MIRRORED_REPEAT:
					case GL_REPEAT:
						texture->wrapT = param;
						break;

					default:
#ifndef NDEBUG
						glSetError(GL_INVALID_ENUM);
#endif
						return;
				}
				break;

			default:
#ifndef NDEBUG
				glSetError(GL_INVALID_ENUM);
#endif
				return;
		}
	}
}

void glTexEnvi(GLenum target, GLenum pname, GLint param) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isTexEnvTarget(target) || !isTexEnvParamName(pname));
	GL_CHECK(GL_INVALID_VALUE, (pname == GL_RGB_SCALE || pname == GL_ALPHA_SCALE) && param != 1 && param != 2 && param != 4);


	TextureUnit * const textureUnit = getActiveTextureUnit();

	switch(target) {
		case GL_TEXTURE_ENV:
			if(pname == GL_TEXTURE_ENV_MODE) {
#ifndef NDEBUG
				if(param == GL_ADD || param == GL_MODULATE || param == GL_DECAL || param == GL_BLEND || param == GL_REPLACE || param == GL_COMBINE) {
#endif
					textureUnit->envMode = param;
#ifndef NDEBUG
				}
				else {
					glSetError(GL_INVALID_ENUM);
					return;
				}
#endif
			}
			else {
#ifndef NDEBUG
				/// \todo - Implement more parameter names.
				glSetError(GL_KOS_UNIMPLEMENTED);
#endif
				return;
			}
			break;

		default:
#ifndef NDEBUG
			/// \todo - Implement more targets.
			glSetError(GL_KOS_UNIMPLEMENTED);
#endif
			return;
	}
}

void glTexEnvf(GLenum target, GLenum pname, GLfloat param) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isTexEnvTarget(target) || !isTexEnvParamName(pname));
	GL_CHECK(GL_INVALID_VALUE, (pname == GL_RGB_SCALE || pname == GL_ALPHA_SCALE) && param != 1 && param != 2 && param != 4);


	glTexEnvi(target, pname, param);
}

GLboolean GLAPIENTRY glIsTexture(GLuint texture) {
	if(texture > 0 && texture <= TEXTURE_MAX_COUNT
		&& isTextureIDUsed(texture)
		&& getTextureByID(texture)->target != GL_NONE) {
		return GL_TRUE;
	}
	else {
		return GL_FALSE;
	}
}
