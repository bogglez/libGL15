/**
 * @file texture.h
 * OpenGL texture and texture unit functions.
 */

#pragma once


typedef struct {
	GLenum    target;
	GLint     border;
	GLenum    format;
	GLenum    type;
	GLint     filter;
	GLuint    width, height;
	GLuint    internalFormat;
	GLvoid   *data;

	// parameters
	/// \todo - Some texture parameters can actually differ between different LOD levels! See glGetTexLevelParameter
	GLint minFilter;
	GLint magFilter;
	GLint minLOD;
	GLint maxLOD;
	GLint baseLevel;
	GLint maxLevel;
	GLint wrapS;
	GLint wrapT;
	GLint wrapR;
	GLfloat borderColor[4];
	GLfloat priority;
	GLint compareMode;
	GLint compareFunc;
	GLint depthTextureMode;
	GLint generateMipMap;
} Texture;

typedef struct {
	GLuint boundTextureID;
	GLenum envMode;
	GLfloat envColor[4];
	GLfloat lodBias;
	GLenum filterControl;
} TextureUnit;


/*
 * Functions
 */

/**
 * Returns the number of the currently active texture unit.
 */
unsigned getActiveTextureUnitNumber();

/**
 * Returns the active texture unit
 */
TextureUnit * getActiveTextureUnit();

/**
 * Initializes all texture state.
 * Used during context creation.
 */
void initTextures();

/**
 * Initializes all texture unit state.
 * Used during context creation.
 */
void initTextureUnits();

/**
 * Converts an OpenGL texture format enum to the corresponding PVR type.
 * @param textureFormat The OpenGL texture format enum.
 * @return The PVR texture format.
 */
unsigned convertGLTextureFormatToPVR(GLenum textureFormat);
