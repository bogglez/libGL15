# OpenGL library for Dreamcast by bogglez. #

This code is based upon the work of

* PH3NOM, BlueCrab and other KOS contributors
* FreeGLUT (glut.h)
* Khronos (parts of glext.h in gl.h)
* Mesa 3D (gl.h, glu.h)

Thanks!

The goal is to support OpenGL 1.5 core and supportable extensions while being as standards compliant as possible.
OpenGL 2.0 core is not possible due to shaders.
