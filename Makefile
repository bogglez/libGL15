INSTALL=install
INSTALL_DATA=${INSTALL} -m 644
INSTALL_PROGRAM=$(INSTALL)

prefix=$(KOS_PORTS)
exec_prefix=$(prefix)
includedir=$(prefix)/include
libdir=$(exec_prefix)/lib

CC       = $(KOS_CC)
ARFLAGS  = rcs
CPPFLAGS = $(filter -D% -I%,$(KOS_CFLAGS)) -I$(srcdir)include
CFLAGS   = $(filter-out -D% -I%,$(KOS_CFLAGS)) -std=gnu11 -Ofast
LDFLAGS  = $(KOS_LDFLAGS) $(KOS_LIB_PATHS)
LDLIBS   = $(KOS_LIBS)

# Setting this define is DANGEROUS!
# It will turn off many error checks in order to reduce code size and branching.
# Only activate it if you tested your application thoroughly!
# Make sure to use glGetError() in your debug builds.
# Size benefits (as of 2014-11-30): 58496 to 33100 bytes (stripped, 43% decrease)
#CPPFLAGS += -DNDEBUG

# Enable horizontal anti-aliasing
CPPFLAGS += -DLIBGL15_FSAA

# Disable warnings about deprecated macros and functions like glBegin
# It is recommended that you rewrite your OpenGL code instead due to performance
# and compatibility problems. E.g. converting glBegin to glDrawArrays gives a huge
# performance speedup.
#CFLAGS += -DLIBGL15_NO_DEPRECATION_WARNINGS


libGL.a: $(patsubst %.c,libGL.a(%.o),$(wildcard $(srcdir)*.c))

clean:
	$(RM) $(wildcard *.o)

distclean:
	$(RM) $(wildcard *.o *.a)

installdirs:
	mkdir -p $(DESTDIR)$(libdir) $(DESTDIR)$(includedir)/GL $(DESTDIR)$(libdir)/pkgconfig

gl.pc: $(srcdir)gl.pc.in
	sed \
		-e "s|@prefix@|$(prefix)|g" \
		-e "s|@exec_prefix@|$(exec_prefix)|g" \
		-e "s|@includedir@|$(includedir)|g" \
		-e "s|@libdir@|$(libdir)|g" \
		-e "s|@VERSION@|1|g" \
		$^ > $@

install: libGL.a gl.pc installdirs
	$(INSTALL_DATA) $(srcdir)include/GL/gl.h $(DESTDIR)$(includedir)/GL
	$(INSTALL_DATA) libGL.a $(DESTDIR)$(libdir)
	$(INSTALL_DATA) gl.pc $(DESTDIR)$(libdir)/pkgconfig

uninstall:
	$(RM) $(addprefix $(DESTDIR)$(includedir)/GL/gl.h)
	-rmdir $(DESTDIR)$(includedir)/GL
