/**
 * @file bufferObject.c
 * OpenGL buffer object related functions (VBO etc).
 */

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
#include "bufferObject.h"
#include "immediate.h"


#define BUFFEROBJECT_TYPE_FIRST GL_ARRAY_BUFFER
#define BUFFEROBJECT_TYPE_LAST  GL_ELEMENT_ARRAY_BUFFER
#define BUFFEROBJECT_TYPE_COUNT ((BUFFEROBJECT_TYPE_LAST) - (BUFFEROBJECT_TYPE_FIRST) + 1)


/*
 * State
 */
GLuint activeBufferObject[BUFFEROBJECT_TYPE_COUNT] = {0};
size_t bufferObjectCount = 0;
BufferObject* bufferObjects = NULL;


/*
 * Internal functions
 */

#ifndef NDEBUG
/**
 * Checks whether the passed GLenum is a buffer object target.
 * @param target The target.
 * @return 1 if valid, 0 otherwise.
 */
static int isValidBufferObjectTarget(GLenum target) {
	return target == GL_ARRAY_BUFFER || target == GL_ELEMENT_ARRAY_BUFFER;
}

/**
 * Checks whether the passed GLenum is a buffer object usage hint.
 * @param usage The usage hint.
 * @return 1 if valid, 0 otherwise.
 */
static int isValidBufferObjectUsage(GLenum usage) {
	return usage == GL_STREAM_DRAW
		|| usage == GL_STREAM_READ
		|| usage == GL_STREAM_COPY
		|| usage == GL_STATIC_DRAW
		|| usage == GL_STATIC_READ
		|| usage == GL_STATIC_COPY
		|| usage == GL_DYNAMIC_DRAW
		|| usage == GL_DYNAMIC_READ
		|| usage == GL_DYNAMIC_COPY;
}
#endif

/**
 * Checks whether the buffer object is currently unused.
 * @param bufferObject The buffer object to be checked.
 * @return 1 if unused, 0 otherwise.
 */
static int isBufferObjectFree(BufferObject const * bufferObject) {
	return bufferObject->state == BUFFEROBJECTSTATE_UNUSED;
}

/**
 * Initializes a buffer object to its default state.
 * @param bufferObject The buffer object to be initializes.
 */
static void initBufferObject(BufferObject * bufferObject) {
	assert(bufferObject);

	bufferObject->state  = BUFFEROBJECTSTATE_UNUSED;
	bufferObject->target = GL_NONE;
	bufferObject->usage  = GL_NONE;
	bufferObject->access = GL_NONE;
	bufferObject->data   = NULL;
	bufferObject->size   = 0;
}

/**
 * Sets which buffer object is currently bound to the given buffer object target.
 * @param target The buffer object target.
 * @param buffer The buffer object ID to be bound.
 */
static void setActiveBufferObjectIDForTarget(GLenum target, GLuint buffer) {
	assert(isValidBufferObjectTarget(target));
	assert(buffer < bufferObjectCount);
	activeBufferObject[target - BUFFEROBJECT_TYPE_FIRST] = buffer;
}

/**
 * Destroys a buffer object by freeing its memory and resetting it to its default state.
 * @param bufferObject The buffer object to be destroyed.
 */
static void destroyBufferObject(BufferObject * bufferObject) {
	assert(bufferObject);
	assert(!isBufferObjectFree(bufferObject));

	bufferObject->state = BUFFEROBJECTSTATE_UNUSED;

	if(bufferObject->data) {
		free(bufferObject->data);
		bufferObject->data = NULL;
	}

	initBufferObject(bufferObject);
}


/*
 * Functions
 */

/**
 * Initializes all buffer objects and determines how many exist.
 * @param requestedBufferObjectCount How many buffer objects to allocate.
 * @return 0 on success, non-zero otherwise.
 */
int initBufferObjects(size_t requestedBufferObjectCount) {
	assert(!bufferObjects);

	bufferObjects = malloc(requestedBufferObjectCount * sizeof(BufferObject));
	if(!bufferObjects) {
		return -1;
	}

	bufferObjectCount = requestedBufferObjectCount;
	for(size_t i = 0; i < bufferObjectCount; ++i) {
		initBufferObject(&bufferObjects[i]);
	}

	return 0;
}

/**
 * Returns the ID of the buffer object bound to the provided buffer object target.
 * @param target The buffer object target for which the ID should be returned.
 * @return The ID of the buffer object bound to the provided target.
 */
GLuint getActiveBufferObjectIDForTarget(GLenum target) {
	assert(isValidBufferObjectTarget(target));
	return activeBufferObject[target - BUFFEROBJECT_TYPE_FIRST];
}

/**
 * Returns the state of the buffer object bound to the provided buffer object target.
 * @param target The buffer object target for which the state should be returned.
 * @return The state of the buffer object bound to the provided target,
 *         or NULL if the ID is invalid.
 */
BufferObject * getActiveBufferObjectForTarget(GLenum target) {
	assert(bufferObjects && isValidBufferObjectTarget(target));

	GLuint const buffer = getActiveBufferObjectIDForTarget(target);
	assert(buffer < bufferObjectCount);

	return buffer ? &bufferObjects[buffer] : NULL;
}

/**
 * Returns the state of the buffer object with the provided ID.
 * @param buffer The ID of the buffer object for which the state should be returned.
 * @return Returns the state of the buffer object with the corresponding ID,
 *         or NULL if the ID is invalid.
 */
BufferObject* getBufferObjectByID(GLuint buffer) {
	if(buffer > 0 && buffer < bufferObjectCount) {
		return &bufferObjects[--buffer];
	}

	return NULL;
}


/*
 * OpenGL functions
 */
void APIENTRY glGenBuffers(GLsizei n, GLuint *buffers) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	unsigned i;
	for(i = 0; i < (unsigned)n && i < bufferObjectCount; ++i) {
		if(!glIsBuffer(i+1)) {
			buffers[i] = i+1;
			getBufferObjectByID(i+1)->state = BUFFEROBJECTSTATE_NEVERBOUND;
		}
	}

	if(i < (unsigned)n) {
#ifndef NDEBUG
		glSetError(GL_OUT_OF_MEMORY); // not standard but makes sense
#endif
		return;
	}
}

void APIENTRY glBindBuffer(GLenum target, GLuint buffer) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidBufferObjectTarget(target));

	assert(buffer < bufferObjectCount);


	// Bind
	if(buffer) {
		BufferObject * const bufferObject = getBufferObjectByID(buffer);
		assert(bufferObject);
		assert(!isBufferObjectFree(bufferObject));

		bufferObject->target = target;
		if(bufferObject->state == BUFFEROBJECTSTATE_NEVERBOUND) {
			bufferObject->usage  = GL_STATIC_DRAW;
			bufferObject->access = GL_READ_WRITE;
		}

		setActiveBufferObjectIDForTarget(target, buffer);
	}

	// "Unbind"/"Bind client memory" (i.e. use vertex arrays)
	else {
		setActiveBufferObjectIDForTarget(target, 0);
	}
}

void APIENTRY glDeleteBuffers(GLsizei n, GLuint const *buffers) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_VALUE, n < 0);


	for(GLsizei i = 0; i < n; ++i) {
		GLuint const buffer = buffers[i];
		BufferObject * bufferObject = getBufferObjectByID(buffer);

		// glDeleteBuffers() ignores invalid IDs
		if(!bufferObject) {
			continue;
		}

		GLuint const activeBufferObject = getActiveBufferObjectIDForTarget(bufferObject->target);

		// Revert binding if buffer to delete is currently bound
		if(activeBufferObject == buffer) {
			setActiveBufferObjectIDForTarget(bufferObject->target, 0);
		}

		destroyBufferObject(bufferObject);
	}
}

GLboolean APIENTRY glIsBuffer(GLuint buffer) {
#ifndef NDEBUG
	if(isImmediateModeActive()) {
		glSetError(GL_INVALID_OPERATION);
		return GL_FALSE;
	}
#endif

	BufferObject const * bufferObject = getBufferObjectByID(buffer);
	return (bufferObject && !isBufferObjectFree(bufferObject)) ? GL_TRUE : GL_FALSE;
}

void APIENTRY glBufferData(GLenum target, GLsizeiptr size, void const *data, GLenum usage) {
	GL_CHECK(GL_INVALID_OPERATION, isImmediateModeActive());
	GL_CHECK(GL_INVALID_ENUM, !isValidBufferObjectTarget(target));
	GL_CHECK(GL_INVALID_ENUM, !isValidBufferObjectUsage(usage));
	GL_CHECK(GL_INVALID_VALUE, size < 0);

	GLuint const buffer = getActiveBufferObjectIDForTarget(target);
	GL_CHECK(GL_INVALID_OPERATION, !buffer);


	BufferObject * bufferObject = getBufferObjectByID(buffer);
	assert(bufferObject);

	if(bufferObject->data) {
		free(bufferObject->data);
		bufferObject->data = NULL;
	}

	bufferObject->data = malloc(size);
	if(!bufferObject->data) {
#ifndef NDEBUG
		glSetError(GL_OUT_OF_MEMORY);
#endif
		return;
	}

	if(data) {
		memcpy(bufferObject->data, data, size);
	}

	bufferObject->size = size;
}
