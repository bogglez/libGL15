/**
 * @file immediate.h
 * OpenGL immediate drawing mode related functions.
 */

#pragma once

/**
 * Checks whether immediate drawing mode is active.
 * Drawing mode is activated by glBegin() and turned off
 * by glEnd().
 * @return 0 if it's off, non-zero if on.
 */
int isImmediateModeActive();
